/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fft/detail/CommonPlan.h"

#include <limits>

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {


CommonPlan::CommonPlan(std::size_t input_data_size)
    : _data_size(input_data_size)
    , _fft_size(input_data_size)
    , _valid(true)
{
}

CommonPlan::CommonPlan(std::size_t input_data_size, std::size_t fft_size)
    : _data_size(input_data_size)
    , _fft_size(fft_size)
    , _valid(true)
{
}

std::size_t CommonPlan::input_size() const
{
    return _data_size;
}

std::size_t CommonPlan::fft_size() const
{
    return _fft_size;
}

bool CommonPlan::operator==(CommonPlan const& other) const
{
    return _fft_size == other._fft_size;
}

void CommonPlan::add(fft::Plan const& plan)
{

    if(plan.fft_size() != _fft_size) {
        _valid = false;         // inconsistent plan provided
        _plans.push_back(plan);
    }
}

void CommonPlan::calculate()
{
    if(_plans.empty()) return;

    // calculate minimum FFT size that is supported by all activated algorithms
    std::size_t min = std::numeric_limits<std::size_t>::max();
    for(Plan const& plan : _plans)
    {
        if(plan.fft_size() < min) {
            min = plan.fft_size();
        }
    }
    _fft_size = min;
    _valid = true;

    // clear vector of added plans once the FFT size is calculated
    _plans.clear();

}

bool CommonPlan::valid() const
{
    return _valid;
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
