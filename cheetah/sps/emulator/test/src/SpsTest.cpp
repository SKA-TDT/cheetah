/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/emulator/test/SpsTest.h"
#include "cheetah/sps/emulator/Sps.h"
#include "cheetah/sps/test_utils/SpsTester.h"
#include "cheetah/generators/GaussianNoiseConfig.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/DispersedPulse.h"
#include "cheetah/utils/ModifiedJulianClock.h"

namespace ska {
namespace cheetah {
namespace sps {
namespace emulator {
namespace test {


SpsTest::SpsTest()
    : ::testing::Test()
    , _config(_ddtr_config)
{
}

SpsTest::~SpsTest()
{
}

void SpsTest::SetUp()
{
}

void SpsTest::TearDown()
{
}

namespace {
typedef sps::CommonTypes<sps::Config, uint8_t> TestSpsTraits;
} // namepsace

TEST_F(SpsTest, test_instantiation)
{
    Sps<TestSpsTraits> testsps(_config);
}

TEST_F(SpsTest, test_candidate_bounds)
{
    panda::PoolResource<cheetah::Cpu> cpu(0);

    typedef data::DedispersionMeasureType<float> Dm;
    _config.ddtr_config().add_dm_range(Dm(0.0 * data::parsecs_per_cube_cm),Dm(1000.0 * data::parsecs_per_cube_cm), Dm(25.0 * data::parsecs_per_cube_cm));
    typedef Sps<TestSpsTraits> SpsType;
    typedef typename TestSpsTraits::value_type NumericalRep;
    SpsType testsps(_config);

    typedef typename SpsType::TimeFrequencyType TimeFrequencyType;
    data::DimensionSize<data::Frequency> number_of_channels(64);
    data::DimensionSize<data::Time> number_of_spectra(46);
    typename SpsType::BufferType buffer(number_of_channels, number_of_spectra + 1);

    typedef typename SpsType::TimeFrequencyType TimeFrequencyType;

    utils::ModifiedJulianClock::time_point start_time(utils::ModifiedJulianClock::duration(6000));

    auto data = TimeFrequencyType::make_shared(number_of_spectra, number_of_channels);
    data->start_time(static_cast<typename TimeFrequencyType::TimePointType>(start_time));
    data->sample_interval(TimeFrequencyType::TimeType(0.0000640000 * boost::units::si::seconds));
    data->set_channel_frequencies_const_width(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz), data::FrequencyType(-1.0 * boost::units::si::mega * data::hz));


    _config.emulator_config().candidate_rate(3397);
    auto it = data->begin();
    buffer.insert(it, data->end(), std::const_pointer_cast<const TimeFrequencyType>(data));

    typedef data::SpCcl<NumericalRep> SpType;

    std::shared_ptr<SpType> sp_data_returned;
    typename SpsType::SpHandler sp_handler = [&](std::shared_ptr<SpType> sp_data) { sp_data_returned = sp_data; };

    for (data::DimensionIndex<data::Time> offset(0); offset < data->number_of_spectra() - 1; ++offset)
    {
        SCOPED_TRACE(offset);
        boost::units::quantity<data::MilliSeconds,double> offset_time((double)offset * data->sample_interval());
        boost::units::quantity<data::MilliSeconds,double> block_end(data->number_of_spectra() * data->sample_interval());

        buffer.offset_first_block(offset * data->number_of_channels());
        typedef data::DmTrials<Cpu,float> DmTrialType;

        testsps(cpu, buffer, sp_handler);

        // Test that the sp_data_returned_object exists
        ASSERT_NE(sp_data_returned.get(), nullptr);

        // Test that the start time of the block is start_time (in MJD)
        ASSERT_EQ(sp_data_returned->start_time(), start_time + offset_time);

        // Check that the offset times are the same
        ASSERT_DOUBLE_EQ(sp_data_returned->offset_time().value(), offset_time.value());
        ASSERT_DOUBLE_EQ(sp_data_returned->offset_time().value(), offset_time.value());

        // Check that the number of candidates returned is correct
        ASSERT_EQ(sp_data_returned->size(), 10);

        // For each cand object in sp_data_returned
        for (auto const& cand : *sp_data_returned)
        {
            // Test that the start time of each candidate generated (in MJD)
            // is greater than the start time of the block
            ASSERT_GE(sp_data_returned->start_time(cand), start_time + offset_time);
            ASSERT_GE(sp_data_returned->start_time(cand), start_time);

            // Test that the start time of the candidate occurs inside the block
            ASSERT_GE(data->start_time() + block_end, sp_data_returned->start_time(cand));

            // Test the DM range
            std::vector<Dm> dm_trials = _config.ddtr_config().dm_trials();
            auto dm_it = std::find (dm_trials.begin(), dm_trials.end(), cand.dm());
            ASSERT_NE(dm_it, dm_trials.end()) << "CAND DM " << cand.dm() << "SIZE " << dm_trials.size();
        }

        sp_data_returned.reset();
    }
}

} // namespace test
} // namespace emulator
} // namespace sps
} // namespace cheetah
} // namespace ska
