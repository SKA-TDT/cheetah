/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <float.h>

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
//__global__ void binInputKernel(float *odata, float *idata, int height, int in_width,
//                               int out_width, int prebin)
//
//
// odata         the output data matrix
// idata         the input data matrix
// in_height     the input matrix number of rows (height = num of freq. channels)
// in_width      the input matrix number of columns (width) equal to the input pitch_dim
// out_width     the output matrix number of columns (width) equal to the output pitch_dim
// prebin        the rebinning factor (2, 4, 8 or 16)
//
//
// This kernel executes the binning of the input matrix.
// When this kernel is executed, the input matrix has been already transposed. In
// this case:
// x direction -> time samples
// y direction -> freq. channels
// The input matrix and output matrix have the same height (4096 channels)
// and different widths. The two widths are not in the 2:1 proportion
// because the dimensionis in the row direction of the in and out matrix are aligned to get
// coalesced i/o operations.

__global__ void binInputKernel(float *odata, float *idata, int in_height, int in_width,
                               int out_width, int prebin)

{
    extern __shared__ float Tile[];      //shared memory size allocated dinamically
    int Tile_dimx = blockDim.x;         // number of threads in x direction
    int Tile_dimy = blockDim.y;         // number of threads in y direction
    //int prebin_index = threadIdx.x % prebin;  // index inside prebinning group
    // OSS: prebin is a power of 2 so modulo operation can be replaced with
    // the next one
    int prebin_index = threadIdx.x & (prebin - 1);  // index inside prebinning group
    float ftemp = 0.;                   // temporary float well

    // null the shared memory
     Tile[threadIdx.x + blockDim.x * threadIdx.y] = 0.;
     __syncthreads();

    int xIndex = blockIdx.x * Tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * Tile_dimy + threadIdx.y;
    int index_in = 0;
    //load data in shared memory
    if ((xIndex < in_width) && (yIndex < in_height)) {
        index_in = xIndex + (yIndex) * in_width;
        Tile[threadIdx.x+ blockDim.x * threadIdx.y] = idata[index_in] ;
    }
    __syncthreads();

    // we only proceed if first thread of prebinning group

    int index_out = 0;
    if (prebin_index == 0) {
        index_out = xIndex/prebin + (yIndex)*out_width;
        // convert to float and sum up
        for (int i = 0 ; i < prebin ; i++) {
            ftemp += Tile[threadIdx.x + i + blockDim.x * threadIdx.y];
        }
        odata[index_out] = ftemp/prebin;
    }
    return;
}

//
// __global__ void transposePreBin_shfl(float *odata, unsigned char *idata, int in_height,
//                                      int out_width, int nsamp, int prebin)
//
// odata         the output data matrix
// idata         the input  data matrix
// in_height     the input  matrix number of rows (i.e. matrix height) equal to the
//               number of freq. channels
// out_width     the output matrix number of cols (i.e. matrix width) equal to the
//               pitch dimension to get aligned  memory data
// nsamp         the number of samples per sub-integration and per channel
//               (<= than input height = rebin[0].pitch_dim)
// prebin        the prebin factor
//
// Executes the transpose of input data and convert it from unsigned to float.
// The rebinning of the input matrix is done using the CUDA _shfl_up
// function.
//
//
__global__ void transposePreBin_shfl(float *odata, unsigned char *idata, int in_height,
                                                 int out_width, int nsamp, int prebin)
{
    extern __shared__ char tile[];      //shared memory size allocated dinamically
    int tile_dimx = blockDim.x;         // number of threads in x direction
    int tile_dimy = blockDim.y;         // number of threads in y direction
    int xIndex = blockIdx.x * tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * tile_dimy + threadIdx.y;
    int id = (gridDim.x * blockDim.x) * yIndex + xIndex; //the global thread id
    int lane_id = id % 32;
    float value = 0.;

    // null the shared memory
     tile[threadIdx.x + blockDim.x * threadIdx.y] = 0;
    __syncthreads();
    int index_in = xIndex + (yIndex)*in_height;
    //load data into shared memory
    if( (xIndex < in_height) && (yIndex < nsamp)) {
        tile[threadIdx.y + blockDim.y * threadIdx.x] = idata[index_in] - 128;
    }
    __syncthreads();
    //rebin data using shfl_up
    value =  (float)tile[threadIdx.x + blockDim.x * (threadIdx.y)];
    for (int i= 1; i<= prebin/2; i*= 2) {
#if __CUDACC_VER_MAJOR__ >= 9
        float n = __shfl_up_sync(0xFFFFFFFF,value, i, 32);
#else
        float n = __shfl_up(value, i, 32);
#endif
        if (lane_id >= i) {
            value += n;
        }
    }
    //store the rebinned values into the output array
    if (((lane_id + 1) & (prebin - 1)) == 0) {
        xIndex = blockIdx.y * tile_dimy + threadIdx.x;
        yIndex = blockIdx.x * tile_dimx + threadIdx.y;
        int index_out = xIndex/prebin + (yIndex)*out_width;
        odata[index_out] = value/prebin;
    }
    return;
}


//__global__ void binInputKernel_shfl(float *odata, float *idata, int in_height,
//                                    int in_width, int out_width, int prebin)
//
// odata         the output data matrix
// idata         the input data matrix
// in_width      the input matrix number of columns (width) equal to the input pitch_ dim
// out_width     the output matrix number of columns (width) equal to the output pitch_dim
// prebin        the rebinning factor (2, 4, 8 or 16)
//
//
// This kernel executes the binning of the input matrix.
// When this kernel is executed, the input matrix hase been already transposed. In
// this case:
// x direction -> time samples
// y direction -> freq. channels
// The input matrix and output matrix have the same height (4096 channels)
// and different widths. The two widths are not in the 2:1 proportion
// because the row dimension of the in and out matrix is aligned to get
// coalesced i/o operations.
//
// The rebinning of the input matrix is done using the CUDA _shfl_up
// function.
//
__global__ void binInputKernel_shfl(float *odata, float *idata, int in_width,
                                    int out_width, int prebin)

{
    float value = 0.;                   // input value
    int xIndex = (blockIdx.x * blockDim.x) + threadIdx.x;
    int yIndex = (blockIdx.y * blockDim.y) + threadIdx.y;
    int id = (gridDim.x * blockDim.x) * yIndex + xIndex; //the global thread id
    int lane_id = id % 32;

    //index of the element inside the indata array
    int index_in = xIndex + (yIndex) * in_width;
    value = idata[index_in] ;

    // sum prebin values
    for (int i = 1; i <= prebin/2; i*= 2) {
#if __CUDACC_VER_MAJOR__ >= 9
        float n = __shfl_up_sync(0xFFFFFFFF, value, i, 32);
#else
        float n = __shfl_up(value, i, 32);
#endif
        if (lane_id >= i) {
            value += n;
        }
    }
    //index of the element inside the out data array
    int index_out = xIndex/prebin + yIndex * out_width;
    if (((lane_id + 1) % prebin) == 0) {
        odata[index_out] = value/prebin;
    }
    return;
}
} //cuda
} //fldo
} //cheetah
} //ska
