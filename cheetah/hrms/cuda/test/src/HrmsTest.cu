#include "cheetah/hrms/cuda/Hrms.cuh"
#include "cheetah/hrms/test_utils/HrmsTester.h"


namespace ska {
namespace cheetah {
namespace hrms {
namespace cuda {
namespace test {

struct CudaTraits
    : public hrms::test::HrmsTesterTraits<hrms::cuda::Hrms::Architecture,hrms::cuda::Hrms::ArchitectureCapability>
{
    typedef hrms::test::HrmsTesterTraits<hrms::cuda::Hrms::Architecture, typename hrms::cuda::Hrms::ArchitectureCapability> BaseT;
    typedef Hrms::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace hrms
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace hrms {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, HrmsTester, CudaTraitsTypes);

} // namespace test
} // namespace hrms
} // namespace cheetah
} // namespace ska