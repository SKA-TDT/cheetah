#include "cheetah/tdas/cuda/Tdas.cuh"
#include "cheetah/cuda_utils/nvtx.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace tdas {
namespace cuda {

template <typename T>
Tdas<T>::Tdas(Config const& config, tdas::Config const& algo_config)
    : utils::AlgorithmBase<Config, tdas::Config>(config, algo_config)
{
}

template <typename T>
Tdas<T>::~Tdas()
{
}

template <typename T>
std::shared_ptr<data::Ccl> Tdas<T>::process(panda::PoolResource<Cuda>& device,
    DmTimeSliceType const& data)
{
    PUSH_NVTX_RANGE("tdas_cuda_Tdas_process",0);
    PANDA_LOG << "cuda::Tdas::process() invoked (on device "<< device.device_id() << ")";
    typedef data::FrequencySeries<cheetah::Cuda,thrust::complex<T>> FourierSeriesType;
    typedef data::TimeSeries<cheetah::Cuda,T> TimeSeriesType;
    typedef data::PowerSeries<cheetah::Cuda,T> PowerSeriesType;

    //Thread safe processing object creation
    //Note that this is memory inefficient but gets us around multi
    //GPU and multi thread issues

    PUSH_NVTX_RANGE("tdas_cuda_Tdas_submodule_creation",0);
    dred::Dred<T> dred(_impl_config.dred_config());
    brdz::Brdz brdz(_impl_config.brdz_config());
    tdrt::Tdrt tdrt(_impl_config.tdrt_config());
    tdao::Tdao tdao(_impl_config.tdao_config());
    pwft::Pwft pwft(_impl_config.pwft_config());
    hrms::Hrms hrms(_impl_config.hrms_config());
    modules::fft::cuda::Fft fft_c2r;
    modules::fft::cuda::Fft fft_r2c;
    POP_NVTX_RANGE; // tdas_cuda_Tdas_submodule_creation

    auto candidate_list_ptr = std::make_shared<data::Ccl>();
    std::size_t nharmonics = _algo_config.number_of_harmonic_sums();
    data::TimeSeries<cheetah::Cuda,float> copy_buffer;
    TimeSeriesType tdrt_input;
    TimeSeriesType tdrt_output;
    FourierSeriesType r2cfft_output;
    FourierSeriesType dred_output;
    PowerSeriesType pwft_output;
    std::vector<PowerSeriesType> hrms_output(nharmonics);


    PUSH_NVTX_RANGE("tdas_cuda_Tdas_process_dm_trial",3);
    for(auto dm_trial_iterator = data.cbegin();
        dm_trial_iterator!=data.cend();
        ++dm_trial_iterator)
    {
        auto const& dm_trial = *dm_trial_iterator;
        //Test if the dm trial meets the minimum criteria for Tdas execution
        if (dm_trial.number_of_samples() < _algo_config.minimum_size())
        {
            PANDA_LOG_WARN << "Number of samples in DM trial is lower than configured minimum size"
            << "("<<dm_trial.number_of_samples()<<" < "<<_algo_config.minimum_size()<<")";
            //Abort Tdas and return empty candidate list
            return candidate_list_ptr;
        }

        //Try to get the DM value from the trial
        data::DedispersionMeasureType<float> const dm = dm_trial.dm();
        PANDA_LOG_DEBUG << "Processing dispersion measure: " << dm;

        //Resize the copy_buffer to the configured Tdas transform size in the config
        copy_buffer.resize(_algo_config.size());

        //Copy single trial from DmTimeSlice to device
        PUSH_NVTX_RANGE("tdas_cuda_Tdas_copy_trial_to_device",1);
        auto it = dm_trial.copy_to(copy_buffer);
        POP_NVTX_RANGE; // tdas_cuda_Tdas_copy_trial_to_device

        //Find how many samples were copied
        std::size_t ncopied = std::distance(copy_buffer.begin(),it);


        PUSH_NVTX_RANGE("tdas_cuda_Tdas_prepare_and_deredden",2);
        //Resize the Tdrt input the size of the valid copied data
        tdrt_input.resize(ncopied);

        //Set the sampling rate on the input
        tdrt_input.sampling_interval(dm_trial.sampling_interval());

        //Copy from the copy buffer to the Tdrt input, implicit uint8_t to T (i.e. float/double) conversion.
        panda::copy(copy_buffer.begin(),copy_buffer.begin()+ncopied,tdrt_input.begin());

        //If the copied size is not the same as the desired transform size it is necessary
        //to pad the data with the mean of the input.
        if (ncopied!=_algo_config.size())
        {
            //Calcute the mean of the valid data
            T padding_value = thrust::reduce(thrust::cuda::par,tdrt_input.begin(),tdrt_input.end())/ncopied;
            //Extent the Tdrt input filling the new memory with the padding value
            tdrt_input.resize(_algo_config.size(),padding_value);
        }

        //Perform forward FFT to prepare data for dereddening
        fft_r2c.process(device,tdrt_input,r2cfft_output);

        //Perform dereddening
        dred.process(device, r2cfft_output, dred_output, _algo_config.acceleration_list_generator().magnitude());

        //Zap birdies
        brdz.process<cheetah::Cuda, T, typename FourierSeriesType::Allocator>(device,dred_output);

        //Transform data back to time domain
        fft_c2r.process(device, dred_output, tdrt_input);

        POP_NVTX_RANGE; // tdas_cuda_Tdas_prepare_and_deredden
        //for each dm retreive the corresponding acceleration list and iterate
        //over each acceleration
        auto acc_list = _algo_config.acceleration_list_generator().acceleration_list(dm,
            tdrt_input.size(),tdrt_input.sampling_interval().value());
        for (auto accel: acc_list)
        {
            PANDA_LOG_DEBUG << "Processing acceleration: " << accel;
            PUSH_NVTX_RANGE("tdas_cuda_Tdas_process_acceleration_trial",4);
            //Resample to the acceleration
            tdrt.process(device,tdrt_input,tdrt_output,accel);

            //Perform forward FFT
            fft_r2c.process(device,tdrt_output,r2cfft_output);

            //Form Spectrum
            pwft.process_nn(device,r2cfft_output,pwft_output);

            //Calculate harmonic sums
            hrms.process(device,pwft_output,hrms_output);

            //Search for signals in the non-summed spectrum
            std::size_t harmonic = 1;
            tdao.process<cheetah::Cuda,T,typename data::PowerSeries<cheetah::Cuda,T>::Allocator>
            (device,pwft_output,*candidate_list_ptr,dm,accel,harmonic);

            //For each harmonically summed spectrum search for significant signals
            ++harmonic;
            for (auto const& series: hrms_output)
            {
                tdao.process<cheetah::Cuda,T,typename data::PowerSeries<cheetah::Cuda,T>::Allocator>
                (device,series,*candidate_list_ptr,dm,accel,harmonic);
                ++harmonic;
            }
            POP_NVTX_RANGE; // tdas_cuda_Tdas_prepare_and_deredden
        }
    }
    POP_NVTX_RANGE; // tdas_cuda_Tdas_process_dm_trial
    PANDA_LOG << "cuda::Tdas::process() complete (on device "<< device.device_id() << ")";
    POP_NVTX_RANGE; //tdas_cuda_Tdas_process
    return candidate_list_ptr;
}

template <typename T>
std::shared_ptr<data::Ccl> Tdas<T>::operator()(panda::PoolResource<Cuda>& device,
    std::shared_ptr<DmTimeSliceType> const& data)
{
    return process(device,*data);
}

template class Tdas<float>;
template class Tdas<double>;

} // namespace cuda
} // namespace tdas
} // namespace cheetah
} // namespace ska
