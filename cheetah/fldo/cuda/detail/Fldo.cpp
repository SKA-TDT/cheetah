/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/fldo/cuda/Fldo.h"
#include "cheetah/fldo/cuda/CommonDefs.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace detail {

#ifdef SKA_CHEETAH_ENABLE_CUDA

template<class FldoTraits>
typename Fldo<FldoTraits>::WorkerType* Fldo<FldoTraits>::RunnerFactory::operator()(ResourceType const& gpu)
{
    BaseT::operator()(gpu);
    return new WorkerType(_config);
}

template<class FldoTraits>
Fldo<FldoTraits>::Fldo(fldo::Config const& config)
    : _cuda_runner(RunnerFactory(config))
{
    //check on input data is done in fldo::Config
}

template<class FldoTraits>
std::shared_ptr<data::Ocld<typename FldoTraits::value_type>> Fldo<FldoTraits>::operator()( ResourceType& gpu
                                                                                         , std::vector<std::shared_ptr<TimeFrequencyType>> const& data
                                                                                         , data::Scl const& candidate_list)
{
    return _cuda_runner(gpu)(gpu, data, candidate_list);
}

#endif //SKA_CHEETAH_ENABLE_CUDA

} // namespace detail
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska
