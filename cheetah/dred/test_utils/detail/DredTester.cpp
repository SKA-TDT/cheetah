/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/dred/test_utils/DredTester.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/ComplexTypeTraits.h"

#include <iostream>
#include <memory>

namespace ska {
namespace cheetah {
namespace dred {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
DredTesterTraits<ArchitectureTag,ArchitectureCapability,T>::DredTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
dred::Dred<T>& DredTesterTraits<ArchitectureTag,ArchitectureCapability,T>::api()
{
    return _api;
}

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
dred::Config& DredTesterTraits<ArchitectureTag,ArchitectureCapability, T>::config()
{
    return _config;
}

template <typename DeviceType, typename Arch, typename T>
struct ExecTest
{
    inline static void test(DeviceType& device, dred::Dred<T>& api)
    {
        typedef typename data::ComplexTypeTraits<Arch,T>::type ComplexType;
        data::FrequencySeries<Arch,ComplexType> input(0.333 * data::hz);
        data::FrequencySeries<Arch,ComplexType> output;
        data::AccelerationType maximum_acceleration = 100.0 * data::meters_per_second_squared;
        api.process(device,input,output,maximum_acceleration);
    }
};

template <typename TestTraits>
DredTester<TestTraits>::DredTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
DredTester<TestTraits>::~DredTester()
{
}

template<typename TestTraits>
void DredTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void DredTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(DredTester, test_exec)
{
    TypeParam traits;
    auto& config = traits.config();
    auto& api = traits.api();
    ExecTest<typename TypeParam::DeviceType, typename TypeParam::Arch, typename TypeParam::ValueType>::test(device,api);
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(DredTester, test_exec);

} // namespace test
} // namespace dred
} // namespace cheetah
} // namespace ska
