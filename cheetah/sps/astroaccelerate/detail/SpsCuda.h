/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_SPS_ASTROACCELERATE_SPSCUDA_H
#define SKA_CHEETAH_SPS_ASTROACCELERATE_SPSCUDA_H

#include "cheetah/sps/astroaccelerate/detail/SpsCuda.cuh"
#include "cheetah/sps/astroaccelerate/SpsStrategy.h"
#include "cheetah/sps/Config.h"
#include "cheetah/ddtr/astroaccelerate/Ddtr.h"
#include "cheetah/ddtr/astroaccelerate/DedispersionStrategy.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/sps/astroaccelerate/detail/SpsAnalysis.h"

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

/**
 * @brief Single pulse search asynchronous task for CUDA
 *
 */
template <typename SpsTraits, typename OptimizationParameterT=SpsOptimizationParameters<typename SpsTraits::value_type>>
class SpsCuda : private utils::AlgorithmBase<Config, sps::Config>
{
    private:
        typedef utils::AlgorithmBase<Config, sps::Config> BaseT;

    public:
        // mark the architecture this algo is designed for
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<3, 5, panda::nvidia::giga> ArchitectureCapability;

    private:
        typedef typename SpsTraits::value_type DataType;

    public:
        typedef data::TimeFrequency<Cpu, uint8_t> TimeFrequencyType;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;

    public:
        SpsCuda(sps::Config const& config);
        SpsCuda(SpsCuda&&) = default;
        SpsCuda(SpsCuda const&) = delete;
        ~SpsCuda();

        /**
         * @brief call the dedispersion/sps algorithm using the provided device
         */
        template<typename BufferType, typename SpHandler>
        std::shared_ptr<data::DmTrials<Cpu,float>> operator()(panda::PoolResource<cheetah::Cuda>&
                                                                   , BufferType&
                                                                   , SpHandler&
                                                                   , ddtr::astroaccelerate::Ddtr<SpsTraits>&);

        void process_block(ddtr::astroaccelerate::DdtrProcessor<SpsTraits>&, std::vector<float>&);

        void convert_candidates(std::vector<float> &output_candidates
                                , int const &current_range
                                , sps::astroaccelerate::SpsStrategy<Cpu, uint8_t> const& sps_strategy
                                , float sampling_time);

        void create_dm_list(sps::astroaccelerate::SpsStrategy<Cpu, uint8_t> const& sps_strategy, int current_range);

    private:
        std::size_t _samples;
        float _threshold;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        float _max_boxcar_width_in_sec;
        size_t _number_of_candidates;
        size_t _previous_candidates;
        std::vector<float> _candidate_list;
        std::vector<int> _dm_list;
};


} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

#include "cheetah/sps/astroaccelerate/detail/SpsCuda.cpp"
#endif // SKA_CHEETAH_SPS_ASTROACCELERATE_SPSCUDA_H
