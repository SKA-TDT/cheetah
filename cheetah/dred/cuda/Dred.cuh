#ifndef SKA_CHEETAH_DRED_CUDA_DRED_CUH
#define SKA_CHEETAH_DRED_CUDA_DRED_CUH

#include "cheetah/dred/cuda/Config.h"
#include "cheetah/dred/Config.h"
#include "cheetah/dred/cuda/detail/MedianScrunch.cuh"
#include "cheetah/dred/cuda/detail/Normaliser.cuh"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/pwft/Pwft.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "panda/arch/nvidia/DeviceCapability.h"
#include <vector>

namespace ska {
namespace cheetah {
namespace dred {
namespace cuda {

/**
 * @brief      CUDA/Thrust implementation of the Dred algorithm
 *
 * @details     This implementation of Dred uses median of 5 based filtering
 *             to produce a series of increasingly smoothed baseline estimates.
 *             Based on being passed a maximum acceleration parameter the smoothing
 *             window for each frequency in calculated so that acceleration smeared
 *             signals are preserved.
 *
 * @tparam     T     The base type of the complex data to be dereddened
 */
template <typename T>
class Dred: public utils::AlgorithmBase<Config,dred::Config>
{
    private:
        typedef data::PowerSeries<cheetah::Cuda,T> SeriesType;
        typedef typename data::ComplexTypeTraits<cheetah::Cuda,T>::type ComplexType;

        /**
         * @brief      Boundaries between median of 5 regimes (median filter widths).
         */
        struct Boundary
        {
            float frequency;
            std::size_t idx;
            std::size_t window;
        };

    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability;
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        /**
         * @brief      Create a new Dred instance
         *
         * @param      impl_config  The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Dred(Config const& impl_config, dred::Config const& algo_config);
        Dred(Dred const&) = delete;
        Dred(Dred&&) = default;
        ~Dred();

        /**
         * @brief      Deredden a complex fourier spectrum
         *
         * @param      resource              The resource to process on
         * @param      input                 The input
         * @param      output                The output
         * @param[in]  maximum_acceleration  The maximum acceleration
         *
         * @tparam     Alloc                 The allocator type of the input and output
         */
        template <typename Alloc>
        void process(panda::PoolResource<cheetah::Cuda>& resource,
            data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc>const& input,
            data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc>& output,
            data::AccelerationType maximum_acceleration);

    private:
        /**
         * @brief      Wrapper for preparing internal buffers
         */
        template <typename Alloc>
        void prepare(data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc> const& input,
            data::FrequencySeries<cheetah::Cuda, ComplexType, Alloc>& output,
            data::AccelerationType maximum_acceleration);
        void median_scrunch5(SeriesType const& in, SeriesType& out);
        void linear_stretch(SeriesType const& in, SeriesType& out, float step);

    private:
        pwft::Pwft _pwft; //for forming direct power spectrum
        std::vector< SeriesType > _medians; //for median scrunched versions of _power_spectrum
        std::vector< Boundary > _boundaries;  //boundaries between smoothing windows
};

} //cuda
} //dred
} //cheetah
} //ska

#include "cheetah/dred/cuda/detail/Dred.cu"

#endif //SKA_CHEETAH_DRED_CUDA_DRED_CUH
