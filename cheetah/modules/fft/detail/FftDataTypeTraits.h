/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_FFTDATATYPETRAITS_H
#define SKA_CHEETAH_MODULES_FFT_FFTDATATYPETRAITS_H

#include "panda/TypeTag.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/data/DefaultAllocator.h"
#include "cheetah/data/DataTypeTraits.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @brief determine the return type of a fft transform (conversion to real types) from the input type given
 * @param ReturnTypeTmpl is a template that generates the return type for the Arch passed as atemplate paramter
 * @param type : the return type that corresponds to the same architecture as that provided
 */
template<typename T>
struct FftInverseType;

template<class Arch, typename ValueType, typename Allocator>
struct FftInverseType<data::FrequencySeries<Arch, ValueType, Allocator>>
{
    template<class ReturnArch>
    using ReturnTypeTmpl = data::TimeSeries<ReturnArch, typename data::IsComplexNumber<ValueType>::value_type>;

    typedef ReturnTypeTmpl<Arch> type;
};

template<class Arch, typename ValueType, typename Allocator>
struct FftInverseType<data::TimeSeries<Arch, ValueType, Allocator>>
{
    template<class ReturnArch>
    using ReturnTypeTmpl = data::FrequencySeries<ReturnArch, typename data::IsComplexNumber<ValueType>::value_type>;
    typedef ReturnTypeTmpl<Arch> type;
};

/**
 * @brief determine the return type of a fft transform (conversion to complex types) from the input type given
 * @param ReturnTypeTmpl is a template that generates the return type for the Arch passed as atemplate paramter
 * @param type : the return type that corresponds to the same architecture as that provided
 */
template<typename T>
struct FftInverseComplexType;

template<class Arch, typename ValueType, typename Allocator>
struct FftInverseComplexType<data::FrequencySeries<Arch, ValueType, Allocator>>
{
    template<class ReturnArch>
    using ReturnTypeTmpl = data::TimeSeries<ReturnArch, typename data::ComplexTypeTraits<ReturnArch, typename data::IsComplexNumber<ValueType>::value_type>::type>;
    typedef ReturnTypeTmpl<Arch> type;
};

template<class Arch, typename ValueType, typename Allocator>
struct FftInverseComplexType<data::TimeSeries<Arch, ValueType, Allocator>>
{
    template<class ReturnArch>
    using ReturnTypeTmpl = data::FrequencySeries<ReturnArch, typename data::ComplexTypeTraits<ReturnArch, typename data::IsComplexNumber<ValueType>::value_type>::type>;
    typedef ReturnTypeTmpl<Arch> type;
};

/**
 * @brief SFINAE helper to detrmine if a Fft data type has a specific Architecture
 */
template<class ArchT, class DataT, typename T = void>
using EnableIfHasArch = data::EnableIfHasArch<ArchT, DataT, T>;

/**
 * @brief SFINAE helper to detrmine if a Fft data type does not have a specific Architecture
 */
template<class ArchT, class DataT, typename T = void>
using EnableIfNotHasArch = data::EnableIfNotHasArch<ArchT, DataT, T>;

/**
 * @brief replace Arch tag in a Fft data type
 */
template<class ReplacementArchT, typename DataT, typename Enable=void>
struct FftReplaceArch
{
    typedef DataT type;
};

template<typename ReplacementArchT, class ArchT, typename ValueT, typename AllocatorT>
struct FftReplaceArch<ReplacementArchT, data::TimeSeries<ArchT, ValueT, AllocatorT>>
{
    typedef typename data::ConvertComplexNumber<ReplacementArchT, ValueT>::type ConvertedType;
    typedef typename data::DefaultAllocator<ReplacementArchT, ConvertedType>::type Allocator;
    typedef data::TimeSeries<ReplacementArchT, ConvertedType, Allocator> type;
};

template<typename ReplacementArchT, class ArchT, typename ValueT, typename AllocatorT>
struct FftReplaceArch<ReplacementArchT, data::FrequencySeries<ArchT, ValueT, AllocatorT>>
{
    typedef typename data::ConvertComplexNumber<ReplacementArchT, ValueT>::type ConvertedType;
    typedef typename data::DefaultAllocator<ReplacementArchT, ConvertedType>::type Allocator;
    typedef data::FrequencySeries<ReplacementArchT, ConvertedType, Allocator> type;
};

template<class ReplacementArchT, class T>
using FftReplaceArch_t = typename FftReplaceArch<ReplacementArchT, T>::type;

/**
 * @brief standard interface to construct an Fft type irrespective of the device type
 */
template<typename DataT, class ArchT, typename... Args>
DataT* create_fft_data(panda::PoolResource<ArchT>& device, Args&&... args)
{
    return data::create_data_type<DataT*>(device, std::forward<Args>(args)...);
}

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FFT_FFTDATATYPETRAITS_H
