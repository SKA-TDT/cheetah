/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_CONFIG_H
#define SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_CONFIG_H

#include "cheetah/utils/Config.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

/**
 * @brief config for gpu_bruteforce DDTR algorithm
 */

class Config : public utils::Config
{
    public:
        Config();
        ~Config();

        /**
         * @brief return true if this algorithm is to be used
         */
        bool active() const;

         /**
         * @brief set the activation value
         */
        void active(bool);

        /**
         * @brief return true if DmTrials is selected to be copied to host(cpu)
         */
        bool copy_dmtrials_to_host() const;

        /**
         * @brief set true if the DmTrials need to be copied to host(cpu)
         */
        void copy_dmtrials_to_host(bool state);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _active;
        bool _copy_dmtrials_to_host;
};


} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_CONFIG_H
