/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/tdao/Config.h"


namespace ska {
namespace cheetah {
namespace tdao {

Config::Config()
    : cheetah::utils::Config("tdao")
    , _threshold(9)
    , _min_frequency(0.0f * data::hz)
    , _max_frequency(5000.0f * data::hz)
{
}

Config::~Config()
{
}

cuda::Config const& Config::cuda_config() const
{
    return _cuda_config;
}

float Config::significance_threshold() const
{
    return _threshold;
}

void Config::significance_threshold(float val)
{
    _threshold = val;
}

data::FourierFrequencyType Config::minimum_frequency() const
{
    return _min_frequency;
}

void Config::minimum_frequency(data::FourierFrequencyType const& min_freq)
{
    _min_frequency = min_freq;
}

data::FourierFrequencyType Config::maximum_frequency() const
{
    return _max_frequency;
}

void Config::maximum_frequency(data::FourierFrequencyType const& max_freq)
{
    _max_frequency = max_freq;
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{

    add_options
    ("significance_threshold", boost::program_options::value<float>(&_threshold)->default_value(9.0f),
        "The significance threshold for candidate to be selected (single trial significance).")
    ("minimum_search_frequency", boost::program_options::value<float>()
        ->default_value(1.0f)
        ->notifier(
            [&](float f)
            {
                _min_frequency = f * data::hz;
            }
        ),
        "The minimum Fourier frequency to search for pulsed signals at (in Hz).")
    ("maximum_search_frequency", boost::program_options::value<float>()
        ->default_value(2000.0f)
        ->notifier(
            [&](float f)
            {
                _max_frequency = f * data::hz;
            }
        ),
        "The maximum Fourier frequency to search for pulsed signals at (in Hz).");
}

} // namespace tdao
} // namespace cheetah
} // namespace ska
