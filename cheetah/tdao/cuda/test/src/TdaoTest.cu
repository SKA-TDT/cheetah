#include "cheetah/tdao/cuda/Tdao.cuh"
#include "cheetah/tdao/test_utils/TdaoTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace tdao {
namespace cuda {
namespace test {

struct CudaTraits : public tdao::test::TdaoTesterTraits<tdao::cuda::Tdao::Architecture,tdao::cuda::Tdao::ArchitectureCapability>
{
    typedef tdao::test::TdaoTesterTraits<tdao::cuda::Tdao::Architecture, typename tdao::cuda::Tdao::ArchitectureCapability> BaseT;
    typedef Tdao::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace tdao
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace tdao {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, TdaoTester, CudaTraitsTypes);
 
} // namespace test  
} // namespace tdao
} // namespace cheetah
} // namespace ska  