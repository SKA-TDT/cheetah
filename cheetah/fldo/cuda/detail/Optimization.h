/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_TEST_UTILS_OPTIMIZATION_H
#define SKA_CHEETAH_FLDO_TEST_UTILS_OPTIMIZATION_H

#include "cheetah/Configuration.h"
#ifdef SKA_CHEETAH_ENABLE_CUDA

#include "FldoUtils.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace util {

// routines defined in Optimization.cu
void corner_turner_and_rebin(cudaDeviceProp properties, int first_bin_idx, uint64_t nsamp_per_subslot,
                             size_t nchannels, std::vector<CandidateRebin> &rebin, unsigned char *d_in);

void build_scrunched_profiles(size_t ncandidates, size_t max_phases, size_t nsubbands, size_t nsubint, float mean,
                              float *d_folded, float *d_weight, float *d_outfprof, float* d_outprof,
                              std::vector<util::GpuStream>&exec_stream);

int first_valid_binidx(std::vector<util::CandidateRebin> &rebin);

void perturb_candidates(data::Scl const& ori_data, data::Scl &opt_data, float *d_folded, float* d_perturbed,
                        float *d_outfprof, float *d_opt_profile, std::vector<util::GpuStream>& exec_stream,
                        std::vector<double> const &time_sum, std::vector <double> const &freq_sum,
                        std::vector<float> &trial_param, int nchannel, int nsubints,
                        int nsubbands, int default_max_phases, double tobs, int p_trial, int p_rf,
                        int pdot_trial, int pdot_rf, int dm_trial, int dm_rf);

void compute_max_sn(size_t const ncandidates, int trials_num, int default_max_phases, float sigma, float
                *d_opt_profile,  std::vector<float>& sn_max, std::vector<float>&
                        pulse_width, std::vector<int>& index_max, std::vector<util::GpuStream>& exec_stream);

void find_max(float *raw_in, int start, int N, float &max_sn, float &pulse_width, int &index);


} // util
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif //SKA_CHEETAH_ENABLE_CUDA

#endif // SKA_CHEETAH_FLDO_TEST_UTILS_OPTIMIZATION_H
