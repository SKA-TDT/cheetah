/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::SpsStrategy(DedispersionStrategyType const& strategy)
    :   _strategy(strategy)
    ,   _candidate_algorithm(1) // 1 for threshold based search (which is basic and selected by default)
    ,   _or_sigma_multiplier(3.0f) // outlier threshold used for the estimating the baseline rms
    ,   _enable_outlier_rejection(1) // enable the outlier rejection to estimate the rms (keep it on)
{
    for (size_t irange = 0; irange < strategy.range(); ++irange) {
        for (size_t ichunk = 0; ichunk < strategy.num_tchunks(); ++ichunk) {
            // NOTE: We are limiting the number candidates to half total number of samples (can go 1/4th)
            _max_candidates += static_cast<size_t>( strategy.ndms()[irange] * strategy.t_processed()[irange][ichunk] / 2 );
        }
    }

    // width of 150ms for binning of 1 and scales with the binning factor
    _max_boxcar_width_desired = static_cast<int>( 0.150f / (strategy.tsamp().value()));
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::~SpsStrategy()
{

}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
size_t SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::max_candidates() const
{
    return _max_candidates;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
size_t SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::max_boxcar_width_desired() const
{
    return _max_boxcar_width_desired;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
std::vector<int> const& SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::boxcar_widths() const
{
    return _boxcar_widths;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
int SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::candidate_algorithm() const
{
    return _candidate_algorithm;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
int SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::current_time_samples() const
{
    return _current_time_samples;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
int SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::iterations() const
{
    return _iterations;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
size_t SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::max_boxcar_width_performed() const
{
    return _max_boxcar_width_performed;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
typename SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::DedispersionStrategyType const& SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::strategy() const
{
    return _strategy;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
float SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::or_sigma_multiplier() const
{
    return _or_sigma_multiplier;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
int SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::enable_outlier_rejection() const
{
    return _enable_outlier_rejection;
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
IterationDetails SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::details(int iteration) const
{
    return _details.at(iteration);
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
int SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::get_boxcar_width(int element) const
{
    std::vector<int> widths{32, 16, 16, 16, 8};
    // NOTE: There is enough elements in the widths list
    if(element < (int)widths.size()) {
        return widths.at(element);
    // NOTE: Not enough elements - get the last element
    }
    else if (widths.size() > 0)
    {
        return widths.back();
    // NOTE: Edge case - the widths vector is empty
    }
    else
    {
        return 0;
    }
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
void SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::generate_processing_details(int current_range, int current_time)
{

    int elements_per_block;
    int elements_rem;

    std::pair<int, size_t> width_limits;
    IterationDetails tmpdetails;

    // NOTE: Need to remove previous processing details
    _details.clear();
    size_t dedispersed_samples = _strategy.t_processed()[current_range][current_time];
    _current_time_samples = _strategy.t_processed()[current_range][current_time];
    if(_max_boxcar_width_desired / _strategy.in_bin()[current_range] > dedispersed_samples)
    {
        width_limits = calculate_max_iterations(dedispersed_samples);
    }
    else
    {
        width_limits = calculate_max_iterations(_max_boxcar_width_desired / _strategy.in_bin()[current_range]);
    }

    _iterations = width_limits.first;
    _max_boxcar_width_performed = width_limits.second;

    generate_boxcars_widths();

    if(_iterations > 0)
    {
        tmpdetails.shift(0);
        tmpdetails.output_shift(0);
        tmpdetails.start_taps(0);
        tmpdetails.iteration(0);

        tmpdetails.decimated_timesamples(dedispersed_samples);
        // NOTE: Each iteration decimates the data by a factor of 2 in the time dimension
        tmpdetails.dtm((dedispersed_samples) >> (tmpdetails.iteration() + 1));
        // NOTE: That creates an even number of decimated time samples
        tmpdetails.dtm(tmpdetails.dtm() - (tmpdetails.dtm() & 1));

        tmpdetails.number_boxcars(get_boxcar_width(0));
        elements_per_block = OptimizationParameterT::PD_NTHREADS * 2 - tmpdetails.number_boxcars();
        tmpdetails.number_blocks(tmpdetails.decimated_timesamples() / elements_per_block);
        elements_rem = tmpdetails.decimated_timesamples() - tmpdetails.number_blocks() * elements_per_block;

        if (elements_rem > 0)
        {
            tmpdetails.number_blocks(tmpdetails.number_blocks()+1);
        }

        // TODO: What's the logic behind this equation?
        tmpdetails.unprocessed_samples(tmpdetails.number_boxcars() + 6);

        if (tmpdetails.decimated_timesamples() < tmpdetails.unprocessed_samples())
        {
            tmpdetails.number_blocks(0);
        }

        tmpdetails.total_unprocessed(tmpdetails.unprocessed_samples());

        _details.push_back(tmpdetails);
    }

    for (int iiter = 1; iiter < _iterations; ++iiter)
    {
        tmpdetails.shift(tmpdetails.number_boxcars() / 2);
        tmpdetails.output_shift(tmpdetails.output_shift() + tmpdetails.decimated_timesamples());
        tmpdetails.start_taps(tmpdetails.start_taps() + tmpdetails.number_boxcars() * (1 << tmpdetails.iteration()));
        tmpdetails.iteration(tmpdetails.iteration() + 1);

        tmpdetails.decimated_timesamples(tmpdetails.dtm());
        tmpdetails.dtm((dedispersed_samples) >> (tmpdetails.iteration() + 1));
        tmpdetails.dtm(tmpdetails.dtm() - (tmpdetails.dtm() & 1));

        tmpdetails.number_boxcars(get_boxcar_width(tmpdetails.iteration()));
        elements_per_block = OptimizationParameterT::PD_NTHREADS * 2 - tmpdetails.number_boxcars();
        tmpdetails.number_blocks(tmpdetails.decimated_timesamples() / elements_per_block);
        elements_rem = tmpdetails.decimated_timesamples() - tmpdetails.number_blocks() * elements_per_block;
        if (elements_rem > 0)
        {
            tmpdetails.number_blocks(tmpdetails.number_blocks()+1);
        }

        tmpdetails.unprocessed_samples(tmpdetails.unprocessed_samples() / 2 + tmpdetails.number_boxcars() + 6);
        if (tmpdetails.decimated_timesamples() < tmpdetails.unprocessed_samples())
        {
            tmpdetails.number_blocks(0);
        }

        tmpdetails.total_unprocessed(tmpdetails.unprocessed_samples() * (1 << tmpdetails.iteration()));
        _details.push_back(tmpdetails);
    }
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
std::pair<int, size_t> SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::calculate_max_iterations(int max_boxcar_width)
{
    int iteration = 0;
    int max_boxcar_width_performed = 0;
    // TODO: This needs some careful condition checking - we can still end up with width greater than the number of time samples
    while (max_boxcar_width_performed < max_boxcar_width)
    {
        max_boxcar_width_performed += get_boxcar_width(iteration) *  (1 << iteration);
        iteration++;
    }
    return std::make_pair(iteration, max_boxcar_width_performed);
}

template<class Arch, typename NumericalRep, typename OptimizationParameterT>
void SpsStrategy<Arch, NumericalRep , OptimizationParameterT>::generate_boxcars_widths()
{
    int current_decimation = 1;
    unsigned width = 0;
    int f = 0;

    while (width < _max_boxcar_width_performed)
    {
        // NOTE: That loops over all possible widths within the current decimation
        for (int b = 0; b < get_boxcar_width(f); b++)
        {
            width = width + current_decimation;
            _boxcar_widths.push_back(width);
        }
        current_decimation *= 2;
        f++;
    }
}

}//astroacclerate
}//sps
}//cheetah
}//ska