/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_SPS_ASTROACCELERATE_SPS_H
#define SKA_CHEETAH_SPS_ASTROACCELERATE_SPS_H

#include "cheetah/Configuration.h"

#ifdef SKA_CHEETAH_ENABLE_ASTROACCELERATE

#include "cheetah/sps/astroaccelerate/detail/SpsCuda.h"
#include "cheetah/sps/detail/CommonTypes.h"
#include "cheetah/ddtr/astroaccelerate/DedispersionPlan.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DmTime.h"
#include "panda/arch/nvidia/DeviceCapability.h"
#include <memory>
#include <type_traits>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

/**
 * @brief Single pulse search asynchronous task for CUDA
 * @details note only uint8_t types are currently supported. All others will cause a runtime exception
 */

namespace detail {

template<class SpsTraits, typename Enable=void>
class Sps
{
    public:
        typedef cheetah::Cpu Architecture;
        typedef astroaccelerate::Config Config;

    public:
        Sps(sps::Config const&) {};

        template<typename SpHandler, typename BufferType>
        std::shared_ptr<typename SpsTraits::DmTrialsType> operator()(panda::PoolResource<cheetah::Cpu>&, BufferType&, SpHandler&);
};

} // namespace detail

template<class SpsTraits>
class Sps : public detail::Sps<SpsTraits>
{
        typedef detail::Sps<SpsTraits> BaseT;

    public:
        typedef typename BaseT::Architecture Architecture;
        typedef typename BaseT::DedispersionPlan DedispersionPlan;
        typedef astroaccelerate::Config Config;

    public:
        Sps(sps::Config const& config);

        template<typename SpHandler, typename BufferType>
        std::shared_ptr<typename SpsTraits::DmTrialsType> operator()(panda::PoolResource<Architecture>& dev, BufferType& buf, SpHandler& sp_h);

};


} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska


#include "cheetah/sps/astroaccelerate/detail/Sps.cpp"
#endif // SKA_CHEETAH_ENABLE_ASTROACCELERATE
#endif // SKA_CHEETAH_SPS_ASTROACCELERATE_SPS_H
