#include "cheetah/modules/fft/cuda/FftPlan.cuh"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace cuda {

template <typename T>
cufftHandle const& FftPlan::plan(FftType fft_type, std::size_t size, std::size_t batch)
{
    cufftType cufft_type = convert_to_cufft_type<T>(fft_type);
    if (!valid(cufft_type,size,batch))
    {
        destroy_plan();
        _size = size;
        _cufft_type = cufft_type;
        _batch = batch;
        CUFFT_ERROR_CHECK(cufftPlan1d(&_plan, (int) _size, _cufft_type, (int) _batch));
    }
    return _plan;
}

} // namespace cuda
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
