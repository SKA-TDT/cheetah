/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/Config.h"
#include "cheetah/fldo/cuda/CommonDefs.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {

//set the default values for:
// - the number of sub-integrations to sum up on input data
// - the number of sub-bands to sum up on input data
static const int default_phases         = fldo::o_phases;
static const unsigned default_nsubints  = fldo::o_subints;
static const unsigned default_nsubbands = fldo::o_bands;
//static uint64_t default_nsamples        = 83886080;    // 2^23

Config::Config()
    : BaseT("origami_cuda", "use the origami cuda based algorithm")
    , _enable_split(true)
    , _nsubints(default_nsubints)
    , _nsubbands(default_nsubbands)
    , _phases(default_phases)
{
}

bool const &Config::enable_split() const
{
    return _enable_split;
}

size_t const& Config::phases() const
{
    return _phases;
}

size_t const& Config::nsubints() const
{
    return _nsubints;
}

size_t const& Config::nsubbands() const
{
    return _nsubbands;
}

void Config::phases(size_t n)
{
    _phases = n;
}

void Config::nsubbands(size_t n)
{
    _nsubbands = n;
}

void Config::nsubints(size_t n)
{
    _nsubints = n;
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    // the add_options method of the class options_description returns a
    // special proxy object  that defines the operator(). Calls to that
    // operator actually declare options.
    BaseT::add_options(add_options);
    add_options("help", " FLDO receives a stream of filterbank data organized as a succession of measures, each comprising a number of frequency channels. \n\
 The folding algorithm consists in a synchronous summation of input data in order to improve the Signal/Noise. Input data is processed in sub-integrations blocks and in frequency group (sub-bands) and summed up coherently, following the pulsar period of the current candidate in an array of maximum length of phases phases. \n\
 doc/FldoParam.md describes the FLDO process and parameters meaning")
    ("enable_split", boost::program_options::value<bool>(&_enable_split)->default_value(_enable_split), "enable folding phase shift")

    // The parameters of operator are: option  name, information about value and option desciprtion
    // The option can specify the address of the varuable where to store the
    // option value and also the default value used if no value is specified
    // by user.
    ("phases", boost::program_options::value<size_t>(&_phases)->default_value(default_phases),
               "specify the max number of phases used in folding")
    ("nsubints",boost::program_options::value<size_t>(&_nsubints)->default_value(default_nsubints),
               "specify the number of sub-integrations the whole scan is divided.")
    ("nsubbands",boost::program_options::value<size_t>(&_nsubbands)->default_value(default_nsubbands),
               "specify the number of frequency sub-bands");

    // check of Config variables
    if (0 > fldo_input_check((*this))) {
        throw panda::Error("Wrong Param Config in FLDO");
    }

}

/**
 * FldoInputCheck(fldo::cuda::Config const& config)
 * @brief
 * verify input config parameter are inside the bounds defined in
 * CommonDefs.h.
 *
 * \return < 0 if error
 */
int Config::fldo_input_check(const fldo::cuda::Config& config)
{
    // We access config variables directly.
    PANDA_LOG_DEBUG << " fldo_input_check: input config parameters check " ;

    // nsubints check
    size_t nsubints = config.nsubints();
    if ((fldo::max_subints < nsubints) || (fldo::min_subints > nsubints)) {
        static constexpr const char* err_msg = "fldo_input_check: invalid nsubints ";
        PANDA_LOG_ERROR << err_msg << " (" << nsubints << ")" ;
        return -3;
    } else {
        PANDA_LOG_DEBUG << "fldo_input_check: nsubints (" << nsubints << ")" ;
    }

    // nsubbands check
    size_t nsubbands = config.nsubbands();
    if ((fldo::max_bands < nsubbands) || (fldo::min_bands > nsubbands)) {
        static constexpr const char* err_msg = "fldo_input_check: invalid nsubbands ";
        PANDA_LOG_ERROR << err_msg << " (" << nsubbands << ")" ;
        //throw panda::Error(err_msg);
        return -4;
    } else {
        PANDA_LOG_DEBUG << "fldo_input_check: nsubbands (" << nsubbands << ")" ;
    }

    // phases check
    // We duplicate inside _nbins
    size_t phases = config.phases();
    //_nbins = phases;   // nbins is an alias for phases
    if ((fldo::max_phases < phases) || (fldo::min_phases > phases)) {
        static constexpr const char* err_msg = "fldo_input_check: invalid phases ";
        PANDA_LOG_ERROR << err_msg << " (" << phases << ")" ;
        //throw panda::Error(err_msg);
        return -5;
    } else {
        PANDA_LOG_DEBUG << "fldo_input_check: phases (" << phases << ")" ;
    }

    return 0;

}

} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska
