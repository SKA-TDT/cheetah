/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fft/test/HasFftProcessMethodTest.h"
#include "cheetah/modules/fft/detail/HasFftProcessMethod.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace test {


HasFftProcessMethodTest::HasFftProcessMethodTest()
    : ::testing::Test()
{
}


namespace {

struct TestArch {};
struct A {
    typedef TestArch Architecture;
};

struct B {
    typedef TestArch Architecture;

    void process( panda::PoolResource<Architecture>&
                , data::TimeSeries<TestArch, float> const&
                , data::FrequencySeries<TestArch, std::complex<float>>&) {}

    void process( panda::PoolResource<Architecture>&
                , data::FrequencySeries<TestArch, float> const&
                , data::TimeSeries<TestArch, double>&) {}
};

struct C {
    typedef TestArch Architecture;

    void process( panda::PoolResource<Architecture>&
                , data::FrequencySeries<TestArch, float> const&
                , data::TimeSeries<TestArch, std::complex<float>>&) {}
};

} // namespace

TEST_F(HasFftProcessMethodTest, test_process_method_time_freq)
{
    static_assert(!HasTime2Frequency<A, float, std::complex<float>>::value, "not detecting missing process method");
    static_assert(HasTime2Frequency<B, float, std::complex<float>>::value, "not detecting process method");
}

TEST_F(HasFftProcessMethodTest, test_process_method_freq_time)
{
    static_assert(!HasFrequency2Time<A, float, std::complex<float>>::value, "not detecting missing process method");
    static_assert(!HasFrequency2Time<B, float, std::complex<float>>::value, "not detecting missing process method");
    static_assert(HasFrequency2Time<C, float, std::complex<float>>::value, "not detecting process method");
}

} // namespace test
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
