/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_ALTERA_FFTWORKER_H
#define SKA_CHEETAH_MODULES_FFT_ALTERA_FFTWORKER_H
#include "cheetah/Configuration.h"

#ifdef SKA_CHEETAH_ENABLE_OPENCL
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "panda/arch/altera/Kernel.h"
#include "panda/arch/altera/CommandQueue.h"
#include "rabbit/cxft/Cxft.h"
#include "rabbit/cxft/opencl/eight_million_point/CxftPlan.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace altera {

/**
 * @brief      A class that calls actual FFT kernels
 *
 * @details    The FftWorker class creates a worker object for a device available in the pool.
 *             This class supports different types of FFTs based on data types like R2C, C2C fwd, C2C inv.
 *
 */
class FftWorker
{
    public:
        typedef typename cheetah::Fpga Architecture;
        template<typename T>
        using Complex = typename data::ComplexTypeTraits<Architecture, T>::type;

    public:
        /**
         * @brief      Construct an FftWorker instance
         *
         * @details    Each worker object is constructed with compatible Rabbit image for that device.
         * @param[in]  device    OpenCl device available in the pool
         *
         */
        FftWorker(panda::PoolResource<Fpga> const& device);

        //Copy constructors removed.
        FftWorker(FftWorker const&) = delete;
        FftWorker(FftWorker&&) = delete;

        /**
         * @brief      Real-to-complex FFT
         *
         * @details    A special case of complex Fast Fourier Transform when the data is real.
         *             This operator processes FFT length of odd power of 2 with two additional kernels.
         *             The range of possible FFTs is defined by Rabbit library linked
         *             first_stge kernel separates the data into even & odd streams.
         *             cxft_kernls() computes complex to complex transform, makes a central F-engine
         *             last_stage kernel uses twiddles generated in Rabbit and discards half of the spectrum
         * @tparam     T                data type, input to FFT
         *             InputAlloc       input data of type T directly allocated onto device buffers
         *             OutputAlloc      output data of complex type directly allocated onto device buffers
         *
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void operator()(data::TimeSeries<cheetah::Fpga , T, InputAlloc> const& input,
                        data::FrequencySeries<cheetah::Fpga, Complex<T>, OutputAlloc>& output) const;

        /**
         * @brief      Complex-to-complex forward FFT
         *
         * @details    A special case of complex Fast Fourier Transform when the data is complex.
         *             This operator processes FFT length of even power of 2 truncating remaining data.
         *             The range of possible FFTs is defined by Rabbit library linked
         *             cxft_kernls() computes complex to complex transform via c2c_transform() method
         * @tparam     T                data type, input to FFT
         * @tparam     InputAlloc       input data of complex type directly allocated onto device buffers
         * @tparam     OutputAlloc      output data of complex type directly allocated onto device buffers
         *
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void operator()(data::TimeSeries<cheetah::Fpga , std::complex<T>, InputAlloc> const& input,
                        data::FrequencySeries<cheetah::Fpga, Complex<T>, OutputAlloc>& output);

        /**
         * @brief      Complex-to-complex inverse FFT
         *
         * @details    A special case of complex Fast Fourier Transform when the data is complex.
         *             This operator processes FFT length of even power of 2 truncating remaining data.
         *             The range of possible FFTs is defined by Rabbit library linked
         *             cxft_kernls() computes complex to complex transform via c2c_transform() method
         * @tparam     T                data type, input to FFT
         * @tparam     InputAlloc       input data of complex type directly allocated onto device buffers
         * @tparam     OutputAlloc      output data of complex type directly allocated onto device buffers
         *
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void operator()(data::FrequencySeries<cheetah::Fpga , std::complex<T>, InputAlloc> const& input,
                        data::TimeSeries<cheetah::Fpga, Complex<T>, OutputAlloc>& output);

    private:
        /**
         * @brief      Complex-to-complex FFT/IFFT
         *
         * @details    This method computes complex to complex transform both forward & inverse FFT
         *             inverse_int flag runs either forward or inverse transform, used in R2C & C2C types.
         *             OpenCL kernels for multiwire architecture are grouped here making a central F-engine
         *
         */
        void cxft_kernels(cl_mem input, cl_mem output, cl_int mangle_int, cl_int twidle_int, cl_int log_rows_arg
                         , cl_int log_columns_arg, cl_int rows_arg, cl_int columns_arg, cl_int inverse_int
                         , float delta_const) const;

        /**
         * @brief      Common function for forward & inverse C2C transform
         *
         * @details    This method does basic input/output preparation based on data size.
         *             It calls cxft_kernls() to get complex to complex transform based on inverse_int flag
         *
         */
        template <typename T, typename InputType, typename OutputType>
        void c2c_transform(InputType const&, OutputType&, cl_int);

    private:
        panda::PoolResource<panda::altera::OpenCl> const& _device;
        std::unique_ptr<panda::altera::Kernel> _fetch_kernel;
        std::unique_ptr<panda::altera::Kernel> _fetch_mwt_kernel;
        std::unique_ptr<panda::altera::Kernel> _fft_kernel;
        std::unique_ptr<panda::altera::Kernel> _transpose_kernel;
        std::unique_ptr<panda::altera::Kernel> _transpose_mwt_kernel;
        std::unique_ptr<panda::altera::Kernel> _last_kernel;
        std::unique_ptr<panda::altera::Kernel> _first_kernel;
        std::unique_ptr<panda::altera::CommandQueue> _first_queue;
        std::unique_ptr<panda::altera::CommandQueue> _fetch_queue;
        std::unique_ptr<panda::altera::CommandQueue> _fetch_mwt_queue;
        std::unique_ptr<panda::altera::CommandQueue> _fft_queue;
        std::unique_ptr<panda::altera::CommandQueue> _transpose_queue;
        std::unique_ptr<panda::altera::CommandQueue> _transpose_mwt_queue;
        std::unique_ptr<panda::altera::CommandQueue> _data_queue;
        std::unique_ptr<panda::altera::CommandQueue> _last_queue;
        ska::rabbit::cxft::Cxft _cxft;
        mutable ska::rabbit::cxft::opencl::eight_million_point::CxftPlan _plan;
};


} // namespace altera
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fft/altera/detail/FftWorker.cpp"

#endif // SKA_CHEETAH_ENABLE_OPENCL
#endif // SKA_CHEETAH_MODULES_FFT_ALTERA_FFTWORKER_H
