/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/astroaccelerate/SpsStrategy.h"
#include "cheetah/data/TimeFrequency.h"
#include "MsdEstimator.h"

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

template<typename SpsTraits, typename OptimizationParameterT>
SpsAnalysis<SpsTraits, OptimizationParameterT>::SpsAnalysis(ddtr::astroaccelerate::DdtrProcessor<SpsTraits>& ddtr_processor
                    , sps::astroaccelerate::SpsStrategy<Cpu, DataType> const& sps_strategy
                    , std::vector<int>& dm_list
                    , std::vector<float>& candidate_list
                    , float threshold
                    , size_t number_of_candidates
                    , size_t max_candidates
                    )
    : _ddtr_processor(ddtr_processor)
    , _sps_strategy(sps_strategy)
    , _msd_estimator(_ddtr_processor, _sps_strategy, (int)_ddtr_processor.current_dm_range())
    , _dm_list(dm_list)
    , _number_of_samples(_sps_strategy.current_time_samples())
    , _d_peak_list(_dm_list[0] * _number_of_samples)
    , _d_decimated((((_dm_list[0] * _number_of_samples)/2)+ OptimizationParameterT::PD_MAXTAPS))
    , _d_boxcar_values(_dm_list[0] * _number_of_samples)
    , _d_output_snr(2*_dm_list[0]*_number_of_samples)
    , _d_output_taps(2*_dm_list[0]*_number_of_samples)
    , _gmem_pos(1)
    , _candidate_list(candidate_list)
    , _number_of_candidates(number_of_candidates)
{
    cudaDeviceSynchronize();
    if (_dm_list.size() > 0)
    {
        cudaMemset((void*) _gmem_pos(), 0, sizeof(int));

        for(int dm_list_index=0; dm_list_index<(int)dm_list.size(); ++dm_list_index)
        {
            cudaDeviceSynchronize();
            perform_msd_search(dm_list_index);

            if (sps_strategy.candidate_algorithm() == 1)
            {
                cudaDeviceSynchronize();
                threshold_based_sps(threshold, dm_list_index);
            }
            else if (sps_strategy.candidate_algorithm() == 0)
            {
                cudaDeviceSynchronize();
                peak_find_sps(threshold, dm_list_index, max_candidates);
            }
            cudaDeviceSynchronize();
            int temp_pos = 0;
            int local_max_list_size = (_dm_list[0]*_number_of_samples) / 4;

            cudaMemcpy(&temp_pos, _gmem_pos(), sizeof(int), cudaMemcpyDeviceToHost);

            if( temp_pos>=local_max_list_size) {
                PANDA_LOG << "WARNING: number of candidates are more than expected. Not all candidates will be saved! Try increassing the Config::sps::astroaccelerate::threshold in the config";
                temp_pos = local_max_list_size;
            }
            if( (unsigned)(temp_pos)<sps_strategy.max_candidates()){
                panda::copy(_d_peak_list.begin(), _d_peak_list.begin()+temp_pos*4, _candidate_list.begin());
                _number_of_candidates = temp_pos;
            } else {
                throw panda::Error("Not enough memory to store all candidates on the host!");
            }

            cudaMemset((void*) _gmem_pos(), 0, sizeof(int));
            cudaDeviceSynchronize();
        }

    } else {
        throw panda::Error("Single pulse search was not run! Not enough memory to search for pulses");
    }

}

template<typename SpsTraits, typename OptimizationParameterT>
SpsAnalysis<SpsTraits, OptimizationParameterT>::~SpsAnalysis()
{
}

template<typename SpsTraits, typename OptimizationParameterT>
size_t SpsAnalysis<SpsTraits, OptimizationParameterT>::number_of_candidates() const
{
    return _number_of_candidates;
}

template<typename SpsTraits, typename OptimizationParameterT>
int SpsAnalysis<SpsTraits, OptimizationParameterT>::perform_msd_search(int dm_list_index)
{
    // CUDA block and CUDA grid parameters
    dim3 grid_size(1, 1, 1);
    dim3 block_size(OptimizationParameterT::PD_NTHREADS, 1, 1);

    // shared memory bank width and cache configuration (not required for latest GPUs)
    cudaDeviceSetCacheConfig (cudaFuncCachePreferShared);
    cudaDeviceSetSharedMemConfig (cudaSharedMemBankSizeEightByte);

    // first iteration
    int msd_plane_pos = 0;
    grid_size.x=_sps_strategy.details(0).number_blocks();
    grid_size.y=_dm_list[dm_list_index];
    grid_size.z=1;

    block_size.x=OptimizationParameterT::PD_NTHREADS;
    block_size.y=1;
    block_size.z=1;

    size_t number_of_samples = _sps_strategy.current_time_samples();
    size_t dm_shift = dm_list_index*_dm_list[0];
    auto d_input_it = _ddtr_processor.gpu_strategy().ddtr_output().begin();
    d_input_it += dm_shift*number_of_samples;

    if(_sps_strategy.details(0).number_blocks()>0)
        ::astroaccelerate::call_kernel_SPDT_GPU_1st_plane(grid_size
                                                        , block_size
                                                        , (thrust::raw_pointer_cast(&(*d_input_it)))
                                                        , _d_boxcar_values()
                                                        , _d_decimated()
                                                        , _d_output_snr()
                                                        , _d_output_taps()
                                                        , (float2 *) _msd_estimator.msd_interpolated()()
                                                        , _sps_strategy.details(0).decimated_timesamples()
                                                        , _sps_strategy.details(0).number_boxcars()
                                                        , _sps_strategy.details(0).dtm());

    for(int iter=1; iter<_sps_strategy.iterations(); ++iter)
    {
        msd_plane_pos = msd_plane_pos + _sps_strategy.details(iter-1).number_boxcars();
        grid_size.x=_sps_strategy.details(iter).number_boxcars(); grid_size.y=_dm_list[dm_list_index]; grid_size.z=1;
        block_size.x=OptimizationParameterT::PD_NTHREADS; block_size.y=1; block_size.z=1;

        if( (iter%2) == 0 )
        {
            if(_sps_strategy.details(iter).number_blocks()>0)
            {
                ::astroaccelerate::call_kernel_SPDT_GPU_Nth_plane(grid_size
                                        , block_size
                                        , &(thrust::raw_pointer_cast(&*(d_input_it))[_sps_strategy.details(iter).shift()])
                                        , &_d_boxcar_values()[_dm_list[dm_list_index]*(number_of_samples>>1)]
                                        , _d_boxcar_values()
                                        , _d_decimated()
                                        , &_d_output_snr()[_dm_list[dm_list_index]*_sps_strategy.details(iter).output_shift()]
                                        , &_d_output_taps()[_dm_list[dm_list_index]*_sps_strategy.details(iter).output_shift()]
                                        , (float2 *) &_msd_estimator.msd_interpolated()()[msd_plane_pos*2]
                                        , _sps_strategy.details(iter).decimated_timesamples()
                                        , _sps_strategy.details(iter).number_boxcars()
                                        , _sps_strategy.details(iter).start_taps()
                                        , (1<<_sps_strategy.details(iter).iteration())
                                        , _sps_strategy.details(iter).dtm());
            }
        }
        else {
            if(_sps_strategy.details(iter).number_blocks()>0)
            {
                ::astroaccelerate::call_kernel_SPDT_GPU_Nth_plane(grid_size
                                        , block_size
                                        , &_d_decimated()[_sps_strategy.details(iter).shift()]
                                        , _d_boxcar_values()
                                        , &_d_boxcar_values()[_dm_list[dm_list_index]*(number_of_samples>>1)]
                                        , thrust::raw_pointer_cast(&*(d_input_it))
                                        , &_d_output_snr()[_dm_list[dm_list_index]*_sps_strategy.details(iter).output_shift()]
                                        , &_d_output_taps()[_dm_list[dm_list_index]*_sps_strategy.details(iter).output_shift()]
                                        , (float2 *) &_msd_estimator.msd_interpolated()()[msd_plane_pos*2]
                                        , _sps_strategy.details(iter).decimated_timesamples()
                                        , _sps_strategy.details(iter).number_boxcars()
                                        , _sps_strategy.details(iter).start_taps()
                                        , (1<<_sps_strategy.details(iter).iteration())
                                        , _sps_strategy.details(iter).dtm());
            }
        }

    }
    return 0;
}

template<typename SpsTraits, typename OptimizationParameterT>
int SpsAnalysis<SpsTraits, OptimizationParameterT>::threshold_based_sps(float threshold, int dm_list_index)
{

    dim3 grid_size(1, 1, 1);
    dim3 block_size(OptimizationParameterT::WARP*OptimizationParameterT::THR_WARPS_PER_BLOCK, 1, 1);

    cudaDeviceSetCacheConfig (cudaFuncCachePreferShared);
    cudaDeviceSetSharedMemConfig (cudaSharedMemBankSizeFourByte);

    int current_range = (int)_ddtr_processor.current_dm_range();
    int output_offset=0;
    int dm_shift = _dm_list[0]*dm_list_index;
    for(int iter = 0; iter < _sps_strategy.iterations(); ++iter)
    {
        if( (_sps_strategy.details(iter).decimated_timesamples() - _sps_strategy.details(iter).unprocessed_samples()) > 0 )
        {
            int elements_per_block = OptimizationParameterT::WARP * OptimizationParameterT::THR_ELEM_PER_THREAD;
            int nblocks = (_sps_strategy.details(iter).decimated_timesamples()-_sps_strategy.details(iter).unprocessed_samples())/elements_per_block;
            int nrest = (_sps_strategy.details(iter).decimated_timesamples()-_sps_strategy.details(iter).unprocessed_samples()) - nblocks*elements_per_block;
            if(nrest>0) ++nblocks;

            int ncudablocks_x = nblocks;
            int ncudablocks_y = _dm_list[dm_list_index]/OptimizationParameterT::THR_WARPS_PER_BLOCK;

            grid_size.x=ncudablocks_x; grid_size.y=ncudablocks_y; grid_size.z=1;
            block_size.x=OptimizationParameterT::WARP*OptimizationParameterT::THR_WARPS_PER_BLOCK; block_size.y=1; block_size.z=1;

            output_offset = _dm_list[dm_list_index] * _sps_strategy.details(iter).output_shift();

            call_kernel_THR_GPU_WARP(grid_size
                                   , block_size
                                   , &_d_output_snr()[output_offset]
                                   , &_d_output_taps()[output_offset]
                                   , _d_peak_list()
                                   , _gmem_pos()
                                   , threshold
                                   , _sps_strategy.details(iter).decimated_timesamples()
                                   , _sps_strategy.details(iter).decimated_timesamples()-_sps_strategy.details(iter).unprocessed_samples()
                                   , dm_shift
                                   , (int)(_dm_list[0]*_number_of_samples/4)
                                   , (1<<iter)
                                   , _sps_strategy.strategy().tsamp().value() * (_sps_strategy.strategy().in_bin()[current_range])
                                   , _sps_strategy.strategy().in_bin()[current_range]
                                   , 0.0);

        }
    }
    return (0);
}

template<typename SpsTraits, typename OptimizationParameterT>
void SpsAnalysis<SpsTraits, OptimizationParameterT>::peak_find_sps(float threshold, int dm_list_index, int max_peak_size)
{

    dim3 block_dim(32, 2, 1);
    dim3 grid_size(1, 1, 1);

    int current_range = (int)_ddtr_processor.current_dm_range();
    float dm_shift = _dm_list[0]*dm_list_index;

    for(int iter=0; iter<_sps_strategy.iterations(); ++iter){
        if( (_sps_strategy.details(iter).decimated_timesamples()-_sps_strategy.details(iter).unprocessed_samples()-1)>0 )
        {
            grid_size.x = 1 + ((_sps_strategy.details(iter).decimated_timesamples()-_sps_strategy.details(iter).unprocessed_samples()-1)/block_dim.x);
            grid_size.y = 1 + ((_dm_list[dm_list_index]-1)/block_dim.y);
            grid_size.z = 1;

            call_kernel_dilate_peak_find(grid_size
                                        , block_dim
                                        , &_d_output_snr()[_dm_list[dm_list_index]*_sps_strategy.details(iter).output_shift()]
                                        , &_d_output_taps()[_dm_list[dm_list_index]*_sps_strategy.details(iter).output_shift()]
                                        , _d_peak_list()
                                        , _sps_strategy.details(iter).decimated_timesamples()
                                        , _dm_list[dm_list_index]
                                        , _sps_strategy.details(iter).unprocessed_samples()
                                        , threshold
                                        , max_peak_size
                                        , _gmem_pos()
                                        , dm_shift
                                        , (1<<iter)
                                        , _sps_strategy.strategy().tsamp().value() * (_sps_strategy.strategy().in_bin()[current_range])
                                        , _sps_strategy.strategy().in_bin()[current_range]
                                        , 0.0);
        }
    }
}

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska
