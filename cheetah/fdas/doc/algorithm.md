@page fdas_algorithm_guide FDAS algorithm guide
# Notes for Developers
## FDAS algorithm
The FDAS module is part of the FDAS pipeline (see cheetah/pipeline/doc/algorithm.md for details of the pipeline). This module specifically performs an acceleration search of the data, and possibly harmonically sums the data as well, though that step can happen separately in the HRMS module.

The acceleration search is intended to remove the effect that line-of-sight acceleration has on the observed spin period of a pulsar, i.e. doppler shift. For a pulsar which is stationary to an observer, the measured spin period will be constant, and will represent the actual spin period of the pulsar. Therefore, the spin frequency (just the inverse of the spin period) will also be constant, and the power of the signal from the pulsar will end up at a well-defined location in the Fourier spectrum, i.e. in one or two frequency bins.

However, if a pulsar is moving away from an observer, the emission will be red-shifted, i.e. the measured spin frequency will be lower than the actual spin frequency of the pulsar. Likewise, for a pulsar moving towards an observer, the emission will be blue-shifted, i.e. the measured spin frequency will be higher than the actual spin frequency.

Think of the following scenario of a pulsar in an orbit around another object. Assume the orbit is "edge-on," i.e. the orbital plane of the pulsar binary system is perpendicular to the plane of the sky as seen by an observer on Earth. Also assume the pulsar is orbiting its companion clockwise when viewed from "above," which here means looking down on the north pole of the Earth. Finally, assume that we start observing the pulsar when it is directly between us and its companion.

Initially, the pulsar will move left in the sky, as seen by the observer, but it will be moving parallel to the plane of the sky, so neither towards nor away from the observer. Therefore, the observer will measure the true spin frequency of the pulsar. As the pulsar moves around the left side of the companion, it will begin to move away from the observer, and its spin frequency will be red-shifted, as measured by the observer. As the pulsar moves behind the companion, it will once again be traveling parallel to the plane of the sky, and the observer will again measure its true spin frequency (if the pulsar is visible behind the companion). Then the pulsar will travel around the right side of the companion and move towards the observer, and its emission will be blue-shifted, so the observer will measure a higher spin frequency. Finally, as the pulsar completes an orbit, it will once again be traveling parallel to the planbe of the sky, and the observer will measure the true spin frequency.

In this scenario, an observer has measured the true spin frequency of the pulsar, as well as lower and higher frequencies. Therefore, the total power in the signal over the orbit of the pulsar will be spread over many frequency bins. This means that the amplitude of the signal in any frequency bin is lower than if all the power was in one or two bins, and therefore the signal might fall below the signal-to-noise threshold, and the pulsar will not be flagged as a candidate. The total number of frequency bins covered will be based on the line-of-sight acceleration of the pulsar around its companion (as well as the spin frequency of the pulsar; see below); a higher acceleration will lead to greater blue- and red-shift, which will cause the signal to spread over more frequency bins:

bins_signal_drifts = acceleration * spin_frequency * (obs_length^2)/c

The width over which the signal spreads is given by:

template_width = (-abs(bins_signal_drifts):abs(bins_signal_drifts)) - bins_signal_drifts/2

The shape of an accelerated pulsar signal in the Fourier domain is defined by the Fresnel integrals S(x) and C(x), and is given by the function:

1/(sqrt(2 * bins_signal_drifts)) * e^(i * pi * (template_width^2)/bins_signal_drifts) * (S(Z) - S(Y) - i * (C(Y) - C(Z)))

where

Y = template_width * sqrt(2/bins_signal_drifts)

Z = (template_width + bins_signal_drifts) * sqrt(2/bins_signal_drifts)

Creating templates with this function for different values of acceleration allows one to match-filter the data (i.e. convolve the data with these function templates), whereby at template of the correct acceleration will return all of the power from an accelerated pulsar in on frequency bin, which could raise the amplitude of the signal above the detection threshold.

Note that each template recovers signals of different spin frequencies at different accelerations; e.g., a template that recovers the power of a ~10 Hz signal accelerated at 300 m/s/s will also recover the power of a 500 Hz signal accelerated at ~10 m/s/s (see the internal memo FDASTestResults for a graphical representation of this).
