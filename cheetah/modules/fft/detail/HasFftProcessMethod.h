/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_HASFFTPROCESSMETHOD_H
#define SKA_CHEETAH_MODULES_FFT_HASFFTPROCESSMETHOD_H

#include "panda/TypeTraits.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"
#include "panda/TupleUtilities.h"


namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/**
 * @file
 * @brief Meta Functions to determine which functionality is provided by an Fft algorithm
 */

/**
 * @brief Meta Function to determine if a type has the process(InputType, OutputType) method
 * @tparam Algorithm  the type to test
 */
template<typename AlgorithmT
        , typename InputType
        , typename OutputType>
class HasFftProcessMethod
{
    private:
        typedef panda::PoolResource<typename AlgorithmT::Architecture> DeviceType;

        template<typename Algo>
        using Has_t = decltype(std::declval<Algo>().process(std::declval<DeviceType&>(),
                                                             std::declval<InputType const&>(),
                                                             std::declval<OutputType&>()
                                                            ));
    public:
        /**
         * @brief set to true if the method exists, false otherwise
         */
        static const bool value = panda::HasMethod<AlgorithmT, Has_t>::value;
};


/**
 * @brief Meta Function to determine if a algorithm has the method
 *  '''process(TimeSeries<InputValueT>, FrequencySeries<OutputValueT>)'''
 */
template<typename AlgorithmT, typename InputValueT, typename OutputValueT>
using HasTime2Frequency = HasFftProcessMethod< AlgorithmT
                                             , data::TimeSeries<typename AlgorithmT::Architecture
                                                               , InputValueT>
                                             , data::FrequencySeries<typename AlgorithmT::Architecture
                                                               , OutputValueT>
                                             >;

/**
 * @brief Meta Function to determine if a algorithm has the method
 *  '''process(FrequencySeries<InputValueT>, TimeSeries<OutputValueT>)'''
 */
template<typename AlgorithmT, typename InputValueT, typename OutputValueT>
using HasFrequency2Time = HasFftProcessMethod< AlgorithmT
                                             , data::FrequencySeries<typename AlgorithmT::Architecture
                                                                    , InputValueT>
                                             , data::TimeSeries<typename AlgorithmT::Architecture
                                                                    , OutputValueT>
                                             >;

} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FFT_HASFFTPROCESSMETHOD_H
