set(MODULE_CUDA_LIB_SRC_CUDA
    src/Hrms.cu
    PARENT_SCOPE
)

set(MODULE_CUDA_LIB_SRC_CPU
    src/Config.cpp
    PARENT_SCOPE
)

add_subdirectory(test)
