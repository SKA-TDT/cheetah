/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FDAS_FDAS_H
#define SKA_CHEETAH_FDAS_FDAS_H

#include "cheetah/fdas/Config.h"
#include "cheetah/data/DmTime.h"
#include "panda/ConfigurableTask.h"

namespace ska {
namespace cheetah {
namespace fdas {

/**
 * @brief
 *    The main API for the FDAS module
 *
 * @details
 *    Will search input DmTime data using whichever resource is
 *    available and the corresponding algorithm for that resource
 */

template<typename Handler>
class Fdas
{
    public:
        typedef data::DmTime<Cpu> DataType;

    public:
        Fdas(ConfigType const&, Handler&);
        ~Fdas();

        /**
         * @brief execute the fdas task on the provided data using whichever resource becomes available first
         *
         * @tparam the Handler will be called when fdas is complete
         *
         * @params[in] input DmTime data to be searched for accelerated pulsar signals
         */
        void operator()(DataType& data);

    private:
        panda::ConfigurableTask<typename Config::PoolType, Handler&, std::shared_ptr<DataType>> _task;
};


} // namespace fdas
} // namespace cheetah
} // namespace ska
#include "cheetah/fdas/detail/Fdas.cpp"

#endif // SKA_CHEETAH_FDAS_FDAS_H 
