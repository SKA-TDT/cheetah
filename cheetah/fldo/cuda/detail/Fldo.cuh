/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CUDA_FLDO_CUH
#define SKA_CHEETAH_FLDO_CUDA_FLDO_CUH

#include "cheetah/fldo/cuda/detail/FldoCuda.h"
#include "cheetah/fldo/Config.h"
#include "panda/DeviceLocal.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace detail {

// Pre-compilable part (i.e. does not depend on template params)
class FldoBase
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::PoolResource<Architecture> ResourceType;

    protected:
        struct RunnerFactory {
            public:
                RunnerFactory(fldo::Config const&);

            protected:
                void operator()(ResourceType const& gpu);

            protected:
                fldo::Config const& _config;
        };

};

/**
 * @brief
 *    The interface for the CUDA FLDO algorithm
 * @details
 */

template<class FldoTraits>
class Fldo : FldoBase
{
    public:
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability; // minimum device requirements
        typedef cuda::Config Config;

    private:
        typedef typename FldoTraits::TimeFrequencyType TimeFrequencyType;
        typedef FldoCuda<typename TimeFrequencyType::value_type> WorkerType;

    public:
        Fldo(fldo::Config const& config);

        /**
         * @brief Performs the folding operation on the provided data
         */
        std::shared_ptr<data::Ocld<typename FldoTraits::value_type>> operator()(ResourceType& device,
                                                  std::vector<std::shared_ptr<TimeFrequencyType>> const& data,
                                                  data::Scl const& input_candidates);

        /**
         * fldo_input_check(fldo::Config const& config)
         * @brief Verify input config parameter are inside the bounds defined in CommonDefs.h.
         *
         * @return < 0 if error
         */
        int fldo_input_check(const fldo::Config & ) ;

    private:
        struct RunnerFactory : public FldoBase::RunnerFactory
        {
                typedef FldoBase::RunnerFactory BaseT;

            public:
                using FldoBase::RunnerFactory::RunnerFactory;
                WorkerType* operator()(ResourceType const& gpu);
        };

    private:
        panda::DeviceLocal<panda::PoolResource<cheetah::Cuda>, RunnerFactory> _cuda_runner;
};

} // namespace detail
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

#include "Fldo.cpp"

#endif // SKA_CHEETAH_FLDO_CUDA_FLDO_CUH
