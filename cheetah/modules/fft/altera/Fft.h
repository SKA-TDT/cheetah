/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_ALTERA_FFT_H
#define SKA_CHEETAH_MODULES_FFT_ALTERA_FFT_H

#include "cheetah/Configuration.h"

#ifdef SKA_CHEETAH_ENABLE_OPENCL
#include "cheetah/modules/fft/altera/detail/FftWorker.h"
#endif // SKA_CHEETAH_ENABLE_OPENCL

#include "cheetah/modules/fft/Plan.h"
#include "cheetah/modules/fft/altera/Config.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/utils/Architectures.h"
#include "panda/DeviceLocal.h"
#include "panda/TypeTag.h"

#ifdef SKA_CHEETAH_ENABLE_OPENCL
namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace altera {

/**
 * @brief
 *      Intel FPGA OpenCL FFT interface
 * @details
 *      Creates a worker object per device available
 *      For this to build and link, the build needs to be configured with the cmake options:
 * @code
 * -DENABLE_OPENCL=true -DENABLE_SKA_RABBIT=true
 * @endocde
 * ##Algorithm Please refer to algorithm.md
 */

class Fft
{
    public:
        typedef typename cheetah::Fpga Architecture;
        template<typename T>
        using ComplexT = typename data::ComplexTypeTraits<Architecture,T>::type;
        typedef altera::Config Config;

    private:
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        Fft(Config const& algo_config);
        Fft(Fft const&) = delete;
        Fft(Fft&&);

        /**
         * @details Real to Complex specialization of the altera FFT
         */
        /**
         * @brief           Real to Complex specialization of the altera FFT
         *
         * @details
         * @param[in]       input           A real TimeSeries instance to be transformed
         * @param[in]       output          A complex FrequencySeries after transform
         * @tparam          T               Input data type
         * @tparam          InputAllocT     The allocator type of the input
         * @tparam          OutputAllocT    The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::TimeSeries<cheetah::Fpga,T,InputAlloc> const& input,
                    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output) const;

        /*
         * @details Frequency Complex to Time Real fft
         * Commented out until we are able to implement this fft transform
         */
        /*
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, InputAlloc> const& input,
                    data::TimeSeries<cheetah::Fpga,T,OutputAlloc>& output);
                    */

        /**
         * @brief           Complex to Complex specialization of the altera forward FFT
         *
         * @details
         * @param[in]       input           A complex TimeSeries instance to be transformed
         * @param[in]       output          A complex FrequencySeries after transform
         * @tparam          T               Input data type
         * @tparam          InputAllocT     The allocator type of the input
         * @tparam          OutputAllocT    The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::TimeSeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
                    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output) const;

        /**
         * @brief           Complex to Complex specialization of the altera inverse FFT
         *
         * @details
         * @param[in]       input           A complex FrequencySeries instance to be transformed
         * @param[in]       output          A complex TimeSeries after transform
         * @tparam          T               Input data type
         * @tparam          InputAllocT     The allocator type of the input
         * @tparam          OutputAllocT    The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::FrequencySeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
                    data::TimeSeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output) const;

        /**
         * @brief           Create FFT Plan, specialization for forward transform
         *
         * @details         returns a plan with suitable FFT size by calling calculate_fft_size() method.
         * @param[in]       input           A complex TimeSeries instance to be transformed
         * @tparam          T1              TimeSeries data type
         * @tparam          T2              FrequencySeries data type
         * @tparam          ArchT1          The architectute of TimeSeries data
         * @tparam          ArchT2          The architectute of FrequencySeries data
         * @tparam          InputAllocT     The allocator type of the input
         * @tparam          OutputAllocT    The allocator type of the output
         */
        template <typename T1, typename T2, typename ArchT1
                 , typename ArchT2, typename InputAllocT, typename OutputAllocT
                 >
        fft::Plan plan(data::TimeSeries<ArchT1, T1, InputAllocT> const& input
                      , panda::TypeTag<data::FrequencySeries<ArchT2, T2, OutputAllocT>> const&) const;

        /**
         * @brief           Create FFT Plan, specialization for inverse transform
         *
         * @details         returns a plan with suitable FFT size by calling calculate_fft_size() method.
         * @param[in]       input           A complex FrequencySeries instance to be transformed
         * @tparam          T1              FrequencySeries data type
         * @tparam          T2              TimeSeries data type
         * @tparam          ArchT1          The architectute of FrequencySeries data
         * @tparam          ArchT2          The architectute of TimeSeries data
         * @tparam          InputAllocT     The allocator type of the input
         * @tparam          OutputAllocT    The allocator type of the output
         */
        template <typename T1, typename T2, typename ArchT1
                 , typename ArchT2, typename InputAllocT, typename OutputAllocT
                 >
        fft::Plan plan(data::FrequencySeries<ArchT1, T1, InputAllocT> const& input
                      , panda::TypeTag<data::TimeSeries<ArchT2, T2, OutputAllocT>> const&) const;

    private:
        /**
         * @details     Calculates FFT size based on R2C or C2C type, internally calls FftSizeHelper struct
         * @tparam      T               Input data type
         * @param[in]   input_size      Input data size
         */
        template<typename T>
        std::size_t calculate_fft_size(std::size_t input_size) const;

    private:
        struct WorkerFactory {
            public:
                WorkerFactory();
                FftWorker* operator()(panda::PoolResource<Architecture> const& device);
        };

    private:
        /**
         * @details     _max_padding is a design paramter that allows zero padding of limited samples.
         */
        static const std::size_t _max_padding;
        mutable panda::DeviceLocal<panda::PoolResource<Architecture>, WorkerFactory> _workers;

};

} // namespace altera
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/fft/altera/detail/Fft.cpp"

#endif // SKA_CHEETAH_ENABLE_OPENCL

#endif // SKA_CHEETAH_MODULES_FFT_ALTERA_FFT_H
