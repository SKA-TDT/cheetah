/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef PWFT_TDT_REFERENCE_H_
#define PWFT_TDT_REFERENCE_H_

#include "cheetah/tdrt/TimeSeries.h"
#include "cheetah/tdrt/FrequencySeries.h"
#include <complex.h>
#include <fftw3.h>

namespace ska {
namespace cheetah {
namespace pwft {
namespace test {

/**
 * @brief
 * Power Spectrum Fourier Transform Reference Model
 *
 * @details
 */
class PwftTDTReference
{
    public:
        PwftTDTReference(size_t length);
    
       ~PwftTDTReference();
    
        void process(tdrt::TimeSeries<float>& h_input,
                     tdrt::FrequencySeries<float>& h_output);

        void sine_signal(float period, tdrt::TimeSeries<float>& h_output);
        
        float pwft_correlation(tdrt::FrequencySeries<float>& h_fsA, tdrt::FrequencySeries<float>& h_fsB);
    
    private:
        size_t         _length;
        fftwf_plan     _plan;
        float*         _fft_in;
        fftwf_complex* _fft_out;
};

} // namespace test
} // namespace pwft
} // namespace cheetah
} // namespace ska

#endif /* PWFT_TDT_REFERENCE_H_ */
