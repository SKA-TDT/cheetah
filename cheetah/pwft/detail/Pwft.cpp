/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pwft/Pwft.h"
#include "panda/TupleUtilities.h"
#include <utility>

namespace ska {
namespace cheetah {
namespace pwft {

template <typename Arch, typename T, typename InputAlloc, typename OutputAlloc, typename... Args>
void Pwft::process_direct(panda::PoolResource<Arch>& resource,
    data::FrequencySeries<Arch,typename data::ComplexTypeTraits<Arch,T>::type,InputAlloc>const& input,
    data::PowerSeries<Arch,T,OutputAlloc>& output,
    Args&&... args)
{
    auto& algo = _implementations.get<Arch>();
    algo.template process_direct<T,InputAlloc,OutputAlloc,Args...>(resource,input,output,std::forward<Args>(args)...);
}

template <typename Arch, typename T, typename InputAlloc, typename OutputAlloc, typename... Args>
void Pwft::process_nn(panda::PoolResource<Arch>& resource,
    data::FrequencySeries<Arch,typename data::ComplexTypeTraits<Arch,T>::type,InputAlloc>const& input,
    data::PowerSeries<Arch,T,OutputAlloc>& output,
    Args&&... args)
{
    auto& algo = _implementations.get<Arch>();
    algo.template process_nn<T,InputAlloc,OutputAlloc,Args...>(resource,input,output,std::forward<Args>(args)...);
}

} // namespace pwft
} // namespace cheetah
} // namespace ska
