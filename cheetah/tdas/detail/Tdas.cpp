/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/tdas/Tdas.h"
#include "panda/Error.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace tdas {

template <typename T>
TdasBase<T>::TdasBase(Config const& config)
    : _config(config)
    , _implementations(cuda::Tdas<T>(_config.cuda_config(),_config))
{
}

template <typename T>
template <typename Arch, typename... Args>
std::shared_ptr<data::Ccl> TdasBase<T>::process(panda::PoolResource<Arch>& resource,
    DmTimeSliceType const& data,
    Args&&... args)
{
    auto& algo = _implementations.template get<Arch>();
    return algo.process(resource, data, std::forward<Args>(args)...);
}

template <typename T>
TdasBase<T>::~TdasBase()
{
}


template <typename T, typename Handler>
Tdas<T,Handler>::Tdas(ConfigType const& config, Handler& handler)
    : TdasBase<T>(config)
    , _config(config)
    , _task(_config.pool(), handler)
{
    if(_config.cuda_config().active()) {
        /**
         * EB: Not clear that this is the correct way to handle this situation.
         * It may be better to extend the Mock class so that it has all the
         * traits expected by the ConfigurableTask internals.
         */
        #ifdef SKA_CHEETAH_ENABLE_CUDA
        PANDA_LOG << "tdas::cuda algorithm activated";
        _task.template set_algorithms<cuda::Tdas<T>>(cuda::Tdas<T>(_config.cuda_config(),_config));
        #else
        throw panda::Error("tdas::cuda algorithm requested on SKA_CHEETAH_ENABLE_CUDA=False build");
        #endif //SKA_CHEETAH_ENABLE_CUDA
    }
    else
    {
        PANDA_LOG_WARN << "No Time Domain Accelerated Search algorithm has been specified";
    }
}

template <typename T, typename Handler>
Tdas<T,Handler>::~Tdas()
{
}

template <typename T, typename Handler>
void Tdas<T,Handler>::operator()(std::shared_ptr<DmTimeType> const& data)
{
    PANDA_LOG_DEBUG << "Tdas::operator() invoked"
                    << "Num blocks in DmTime object: " << (data->blocks()).size();

    for (auto it = data->begin(_config.dm_trials_per_task()); it < data->end(); ++it)
    {
        std::shared_ptr<DmTimeSliceType> slice = *it;
        PANDA_LOG_DEBUG << "First DM in slice: " << (*(slice->cbegin())).dm();
        _task.submit(slice);
        PANDA_LOG_DEBUG << "Tdas::operator() " << _config.dm_trials_per_task() << " DM trials submitted for processing";
    }
}


} // namespace tdas
} // namespace cheetah
} // namespace ska
