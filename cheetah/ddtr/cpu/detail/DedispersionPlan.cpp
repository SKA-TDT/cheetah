/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/cpu/DedispersionPlan.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {

template <typename DdtrTraits>
DedispersionPlan<DdtrTraits>::DedispersionPlan(ConfigType const& config, std::size_t memory)
    : _algo_config(config)
    , _memory(memory)
{
    if(config.dm_trials().size()==0)
    {
        throw panda::Error("ddtr::cpu Please specify DM ranges to dedisperse");
    }
}

template <typename DdtrTraits>
DedispersionPlan<DdtrTraits>::~DedispersionPlan()
{
}

template <typename DdtrTraits>
data::DimensionSize<data::Time> DedispersionPlan<DdtrTraits>::reset(TimeFrequencyType const& data)
{
    Dm max_dm = this->_algo_config.max_dm();

    FrequencyListType const& channel_freqs = data.channel_frequencies();
    auto freq_pair = data.low_high_frequencies();
    FrequencyType freq_top = freq_pair.second;
    FrequencyType freq_bottom = freq_pair.first;
    _max_delay = std::size_t((this->_algo_config.dm_constant().value()
                    * (1.0/(freq_bottom*freq_bottom) -  1.0/(freq_top*freq_top))
                    * max_dm / data.sample_interval()).value()) + 1;
    for (auto freq: channel_freqs)
    {
        double factor = (this->_algo_config.dm_constant().value() * (1.0/(freq*freq) -  1.0/(freq_top*freq_top)) / data.sample_interval()).value();
        _dm_factors.push_back(factor);
    }

    _number_of_spectra = std::min(_memory/data.number_of_channels(), this->_algo_config.dedispersion_samples());
    if (_number_of_spectra < _max_delay * 2)
    {
        PANDA_LOG_WARN << "Requested number of samples to dedisperse ("
                        << this->_number_of_spectra
                        << ") is less than twice the max dispersion delay ("
                        << 2 * _max_delay << ")";
    }
    _dm_trial_metadata = this->_algo_config.generate_dmtrials_metadata(data.sample_interval(), _number_of_spectra, _max_delay);
    return data::DimensionSize<data::Time>(_number_of_spectra);
}

template <typename DdtrTraits>
std::vector<typename DedispersionPlan<DdtrTraits>::Dm> const& DedispersionPlan<DdtrTraits>::dm_trials() const
{
    return _algo_config.dm_trials();
}

template <typename DdtrTraits>
std::vector<double> const& DedispersionPlan<DdtrTraits>::dm_factors() const
{
    return _dm_factors;
}

template <typename DdtrTraits>
data::DimensionSize<data::Time> DedispersionPlan<DdtrTraits>::buffer_overlap() const
{
    return data::DimensionSize<data::Time>(_max_delay);
}

template <typename DdtrTraits>
void DedispersionPlan<DdtrTraits>::reset(data::DimensionSize<data::Time> const& spectra)
{
    _number_of_spectra = spectra;
}

template <typename DdtrTraits>
data::DimensionSize<data::Time> DedispersionPlan<DdtrTraits>::number_of_spectra() const
{
    return data::DimensionSize<data::Time>(_number_of_spectra);
}

template <typename DdtrTraits>
std::shared_ptr<data::DmTrialsMetadata> DedispersionPlan<DdtrTraits>::generate_dmtrials_metadata(typename DedispersionPlan<DdtrTraits>::TimeType sample_interval, data::DimensionSize<data::Time> nspectra, std::size_t nsamples) const
{
    return _algo_config.generate_dmtrials_metadata(sample_interval, nspectra, nsamples);
}

template <typename DdtrTraits>
std::shared_ptr<data::DmTrialsMetadata> DedispersionPlan<DdtrTraits>::dm_trials_metadata(data::TimeFrequencyMetadata const& tf_info, data::DimensionSize<data::Time> number_of_spectra)
{
    if (number_of_spectra != _number_of_spectra) {
        _dm_trial_metadata = this->generate_dmtrials_metadata(tf_info.sample_interval(), number_of_spectra, _max_delay);
        _number_of_spectra = number_of_spectra;
    }
    return _dm_trial_metadata;
}

} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska
