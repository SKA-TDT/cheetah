/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_FFTMODULE_H
#define SKA_CHEETAH_MODULES_FFT_FFTMODULE_H

#include "cheetah/modules/fft/detail/AlgoFactory.h"
#include "cheetah/modules/fft/detail/CommonPlan.h"
#include "cheetah/modules/fft/detail/FftDataTypeTraits.h"
#include "cheetah/modules/fft/detail/FftTraits.h"
#include "cheetah/modules/fft/detail/HandlerWrapper.h"
#include "cheetah/modules/fft/detail/InputDataReshaper.h"
#include "cheetah/modules/fft/Config.h"
#include "cheetah/utils/AlgoModule.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

/*
 * @brief This Module provides the virtual function wrappers around a single method to allow runtime selection of algorithms
 * @details The signatures of the virtual functions are specified in the Traits class, and the utility AlgoModule is used to
 *          do the actual generation. Each Algorithm is wrapped in an AlgoWrapper<> class which manages data routing from the
 *          virtual call to the algorithms primary interface taking into consideration adjustments for the CommonPlan and
 *          generating the correct output types for the async calls.
 */
template<class TraitsT>
class FftSingleMethodModule : public utils::AlgoModule<TraitsT>
{
        typedef utils::AlgoModule<TraitsT> BaseT;
        typedef typename TraitsT::ConfigType Config;
        typedef typename Config::PoolType PoolType;
        typedef typename TraitsT::Handler::Handler Handler;
        typedef typename TraitsT::AlgoFactory AlgoFactory;

        template<class ArchT> using OutputT = typename TraitsT::template OutputTmpl<ArchT>;

    public:
        FftSingleMethodModule(PoolType, Config const&, Handler& handler);
        template<typename InputT, class Arch>
        std::shared_ptr<panda::ResourceJob> submit(std::shared_ptr<InputT> const&) const;

        // synchro call
        template<class ArchT, typename InputDataT, typename OutputDataT>
        void exec(panda::PoolResource<ArchT>&, InputDataT const&,  OutputDataT&) const;

    protected:
        template<typename InputDataT, typename... DataTs>
        CommonPlan plan(InputDataT const& data, DataTs&&...) const;

        template<typename InputDataT, typename... DataTs>
        CommonPlan plan(std::shared_ptr<InputDataT> const& data, DataTs&&...) const;

    private:
        InputDataReshaper _reshaper;
};

/**
 * @brief The top level Asyn module interface composed af various FftSingleMethodModule for each supported interface family
 */
template<class FftTraitsT, class... FftAlgoTs>
class FftModule
{
        template<class ArchT, typename ValueT>
        using ComplexT = typename data::ComplexTypeTraits<ArchT, ValueT>::type;

        typedef typename fft::ConfigType Config;
        typedef typename FftTraitsT::Handler Handler;

        template<typename TraitsT>
        struct FftAlgoTraits : public TraitsT
        {
            typedef HandlerWrapper<TraitsT> Handler;
            typedef fft::AlgoFactory<typename TraitsT::ConfigType> AlgoFactory;
            typedef typename ConfigType::PoolType PoolType;
        };

    public:
        FftModule(Config const&, Handler& handler);

    protected:
        template<class OutputArch
               , class InputArch
               , typename ValueT
               , data::EnableIfIsNotComplexNumber<ValueT, bool> = true>
        std::shared_ptr<panda::ResourceJob> submit(std::shared_ptr<data::TimeSeries<InputArch, ValueT>> const&) const;

        template<class OutputArch
               , class InputArch
               , typename ValueT
               , data::EnableIfIsComplexNumber<ValueT, bool> = true>
        std::shared_ptr<panda::ResourceJob> submit(std::shared_ptr<data::TimeSeries<InputArch, ValueT>> const&) const;

        template<class OutputArch
               , class InputArch
               , typename ValueT
               , data::EnableIfIsComplexNumber<ValueT, bool> = true>
        std::shared_ptr<panda::ResourceJob> submit(std::shared_ptr<data::FrequencySeries<InputArch, ValueT>> const&) const;

        template<class OutputArch
               , class InputArch
               , typename ValueT
               , data::EnableIfIsComplexNumber<ValueT, bool> = true>
        std::shared_ptr<panda::ResourceJob> submit_to_real(std::shared_ptr<data::FrequencySeries<InputArch, ValueT>> const&) const;

        // sync interface
        template<typename Arch
               , typename InputArch , typename InputValT
               , class OutputArch, typename OutputValT
               , data::EnableIfIsNotComplexNumber<InputValT, bool> = true
               , data::EnableIfIsComplexNumber<OutputValT, bool> = true
               >
        void exec(panda::PoolResource<Arch>& res
                , data::TimeSeries<InputArch, InputValT> const& input
                , data::FrequencySeries<OutputArch, OutputValT>& output) const;

        template<typename Arch
               , typename InputArch , typename InputValT
               , class OutputArch
               , data::EnableIfIsComplexNumber<InputValT, bool> = true
               >
        void exec(panda::PoolResource<Arch>&, data::TimeSeries<InputArch, InputValT> const&
                , data::FrequencySeries<OutputArch, InputValT>&) const;

        template<typename Arch
               , typename InputArch , typename InputValT
               , class OutputArch
               , data::EnableIfIsComplexNumber<InputValT, bool> = true
               >
        void exec(panda::PoolResource<Arch>&, data::FrequencySeries<InputArch, InputValT> const&
                , data::TimeSeries<OutputArch, InputValT>&) const;

        template<typename Arch
               , typename InputArch , typename InputValT
               , class OutputArch, typename OutputValT
               , data::EnableIfIsComplexNumber<InputValT, bool> = true
               , data::EnableIfIsNotComplexNumber<OutputValT, bool> = true
               >
        void exec(panda::PoolResource<Arch>& res
                 , data::FrequencySeries<InputArch, InputValT> const& input
                 , data::TimeSeries<OutputArch, OutputValT>& output) const;

    private:
        // Each of these members handles a single method for the Fft interface
        FftSingleMethodModule<FftAlgoTraits<TimeR2CTraits<FftTraitsT, FftAlgoTs...>>> _time2complex;
        FftSingleMethodModule<FftAlgoTraits<TimeC2CTraits<FftTraitsT, FftAlgoTs...>>> _time_complex2complex;
        FftSingleMethodModule<FftAlgoTraits<FreqC2CTraits<FftTraitsT, FftAlgoTs...>>> _freq_complex2complex;
        FftSingleMethodModule<FftAlgoTraits<FreqC2RTraits<FftTraitsT, FftAlgoTs...>>> _freq_complex2real;

};


} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
#include "FftModule.cpp"

#endif // SKA_CHEETAH_MODULES_FFT_FFTMODULE_H
