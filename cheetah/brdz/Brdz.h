/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_BRDZ_BRDZ_H
#define SKA_CHEETAH_BRDZ_BRDZ_H

#include "cheetah/brdz/Config.h"
#include "cheetah/brdz/cuda/Brdz.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/utils/Architectures.h"

#include "panda/AlgorithmTuple.h"

namespace ska {
namespace cheetah {
namespace brdz {

/**
 * @brief      Class for performing birdie zapping
 *
 * @details     This class provides an interface to implementations of the
 *             Brdz module. This module provides simple functionality to
 *             excise frequencies from a spectrum. Frequencies are provided in
 *             the form of a list/vector of Birdie instances.
 */
class Brdz
{
    private:
        typedef panda::AlgorithmTuple<cuda::Brdz> Implementations;

    public:

        /**
         * @brief      Create a new Brdz instance
         *
         * @param      config  The Brdz algorithm configuration
         */
        Brdz(Config const& config);
        Brdz(Brdz const&) = delete;
        Brdz(Brdz&&) = default;
        ~Brdz();

        /**
         * @brief      Excise the power for birdie frequencies in a spectrum
         *
         * @details     This method forwards to a specific implementation based on the Arch
         *             type and the arguments types.
         *
         * @param      resource   The resource to process on
         * @param      input      The input spectrum to excise birdies from
         * @param[in]  args       Additional arguments to be passed to the implementation
         *
         * @tparam     Arch       The architecture to run on
         * @tparam     T          The base type of the input complex spectrum
         * @tparam     Alloc      The allocator of the frequency series
         * @tparam     Args       The types for additional arguments
         */
        template <typename Arch, typename T, typename Alloc, typename... Args>
        void process(panda::PoolResource<Arch>& resource,
            data::FrequencySeries<Arch, typename data::ComplexTypeTraits<Arch,T>::type, Alloc>& input,
            Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;
};


} // namespace brdz
} // namespace cheetah
} // namespace ska
#include "cheetah/brdz/detail/Brdz.cpp"

#endif // SKA_CHEETAH_BRDZ_BRDZ_H
