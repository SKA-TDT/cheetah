include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_cxft_src src/gtest_cxft.cpp)

add_executable(gtest_cxft ${gtest_cxft_src})
target_link_libraries(gtest_cxft ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_cxft gtest_cxft)
