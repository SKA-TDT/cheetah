/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_TDRT_TDRT_H
#define SKA_CHEETAH_TDRT_TDRT_H

#include "cheetah/tdrt/Config.h"
#include "cheetah/tdrt/cuda/Tdrt.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/Architectures.h"

#include "panda/AlgorithmTuple.h"

namespace ska {
namespace cheetah {
namespace tdrt {

/**
 * @brief
 * Time Domain Resampler CUDA version / Transform / Module
 */

class Tdrt
{
    private:
        typedef panda::AlgorithmTuple<cuda::Tdrt> Implementations;

    public:
        /**
         * @brief      Construct a new instance
         *
         * @param      config  A Tdrt configuration object
         */
        Tdrt(Config const& config);
        Tdrt(Tdrt const&) = delete;
        Tdrt(Tdrt&&) = default;
        ~Tdrt();

        /**
         * @brief      Resample a time series to a given acceleration
         *
         * @details    This is a forwarding call that will pass on the given arguments
         *             to the first implementation that provides a matching method.
         *
         * @param      resource      The pool resource on which to process
         * @param      input         The input time series
         * @param      output        The output time series
         * @param[in]  acceleration  The acceleration value to resample to
         * @param[in]  args          Any additional parameters to be forwarded
         *
         * @tparam     Arch          The architecture of the input & output
         * @tparam     T             The value type of the input & output
         * @tparam     Alloc         The allocator type of the input & output
         * @tparam     Args          The types of any additional args to be forwarded
         */
        template <typename Arch, typename T, typename Alloc, typename... Args>
        void process(panda::PoolResource<Arch>& resource,
           data::TimeSeries<Arch,T,Alloc>const& input,
           data::TimeSeries<Arch,T,Alloc>& output,
           data::AccelerationType acceleration,
           Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;
};

} // namespace tdrt
} // namespace cheetah
} // namespace ska

#include "cheetah/tdrt/detail/Tdrt.cpp"

#endif // SKA_CHEETAH_TDRT_TDRT_H
