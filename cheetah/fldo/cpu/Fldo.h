/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CPU_FLDO_H
#define SKA_CHEETAH_FLDO_CPU_FLDO_H

#include "cheetah/fldo/Config.h"
#include "cheetah/fldo/cpu/Config.h"
#include "cheetah/fldo/Types.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cpu {

/**
 * @brief Cpu based pulsar folding algoithm
 * @details
 * TODO The most awesomist cpu based folding algo ever. It works by magic!!!
 */

template<typename FldoTraits>
class Fldo
{
        typedef typename FldoTraits::TimeFrequencyType TimeFrequencyType;

    public: // typedefs
        typedef cheetah::Cpu Architecture;
        typedef cpu::Config Config;
        typedef FldoTraits Traits;

    public: // methods
        Fldo(fldo::Config const& config);

        /**
         * @brief Performs the folding operation on the provided data
         */
        std::shared_ptr<data::Ocld<typename FldoTraits::value_type>> operator()( panda::PoolResource<Cpu>& device
                                                                               , std::vector<std::shared_ptr<TimeFrequencyType>> const& tf_data
                                                                               , data::Scl const& scl_data);

    private: // members
};

} // namespace cpu
} // namespace fldo
} // namespace cheetah
} // namespace ska

#include "cheetah/fldo/cpu/detail/Fldo.cpp"

#endif // SKA_CHEETAH_FLDO_CPU_FLDO_H
