/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/Config.h"
#include "cheetah/ddtr/DedispersionConfig.h"


namespace ska {
namespace cheetah {
namespace sps {

Config::Config(ddtr::Config& ddtr_config)
    : BaseT("sps")
    , _ddtr_config(ddtr_config)
    , _threshold(20.0)
{
}

Config::operator ddtr::DedispersionTrialPlan const& () const
{
    return _ddtr_config;
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("threshold", boost::program_options::value<float>(&_threshold), "single pulse detection threshold in sigmas");
}

float Config::threshold() const
{
    return _threshold;
}

astroaccelerate::Config const& Config::astroaccelerate_config() const
{
    return BaseT::config<astroaccelerate::Config>();
}

astroaccelerate::Config& Config::astroaccelerate_config()
{
    return BaseT::config<astroaccelerate::Config>();
}

emulator::Config const& Config::emulator_config() const
{
    return BaseT::config<emulator::Config>();
}

emulator::Config& Config::emulator_config()
{
    return BaseT::config<emulator::Config>();
}

ddtr::Config& Config::ddtr_config()
{
    return _ddtr_config;
}

ddtr::Config const& Config::ddtr_config() const
{
    return _ddtr_config;
}

std::size_t Config::dedispersion_samples() const
{
    return _ddtr_config.dedispersion_samples();
}

} // namespace sps
} // namespace cheetah
} // namespace ska
