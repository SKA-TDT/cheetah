/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPS_SPS_H
#define SKA_CHEETAH_SPS_SPS_H

#include "cheetah/sps/Config.h"
#include "cheetah/sps/detail/CommonTypes.h"
#include "cheetah/sps/detail/SpsModule.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/sps/emulator/Sps.h"
#include "cheetah/sps/astroaccelerate/Sps.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace sps {

/**
 * @brief algorithms listed here will be made avaialble via the top level Sps API
 */
template<typename ConfigType, typename NumericalT>
using SpsAlgos=SpsModule<CommonTypes<ConfigType, NumericalT>
                               , emulator::Sps
#ifdef SKA_CHEETAH_ENABLE_ASTROACCELERATE
                               , astroaccelerate::Sps
#endif // SKA_CHEETAH_ENABLE_ASTROACCELERATE
                               >;

/**
 * @brief SPS module top level API.
 * @details Exposes all available SPS algorithms to the user for selection via runtime configuration options
 */

template<class ConfigType, typename NumericalT>
class Sps : public SpsAlgos<ConfigType, NumericalT>
{
        typedef SpsAlgos<ConfigType, NumericalT> BaseT;
        typedef CommonTypes<ConfigType, NumericalT> SpsTraits;

    public:
        typedef data::TimeFrequency<Cpu, NumericalT> TimeFrequencyType;
        typedef typename SpsTraits::SpType SpType;
        typedef typename SpsTraits::SpHandler SpHandler;
        typedef typename SpsTraits::DedispersionHandler DedispersionHandler;
        typedef DedispersionHandler DmHandler;
        typedef typename SpsTraits::DmTrialsType DmTrialType;

    public:
        /**
         * @brief Constructor takes two Handlers
         * @details passes the arguments to BaseT
         * @param dedispersion_handler A functor to be called with the shared pointer to resultant DmTrials object with a signature of
         * '''void(std::shared_ptr<data::DmTrials<Cpu, float>>)'''
         * @param sp_handler A functor to be called with the Sps results. Its signature should be '''void(std::shared_ptr<data::SpCcl<NumericalT>>)'''.
         */
        Sps(ConfigType const& config, DedispersionHandler const& dedispersion_handler, SpHandler const& sp_handler);
        ~Sps();

        /**
         * @brief dedisperses chunk of buffer data to a dm-time chunk.
         *
         * @details the DedispersionHandler will be called when dedispersion is complete. The method
         *         is delegated to device specific implementations.
         *
         * @param input A TimeFequency data type (or equivalent) of data to dedisperse.
         */
        template<typename TimeFreqDataT
                , typename data::EnableIfIsTimeFrequency<TimeFreqDataT, bool> = true>
        void operator()(TimeFreqDataT const& input);

        /**
         * @brief dedisperses chunk of buffer data to a dm-time chunk.
         *
         * @details the DedispersionHandler will be called when dedispersion is complete. The method
         *         is delegated to device specific implementations.
         *
         * @param data A shared_ptr of TimeFrequency data type (or equivalent) of data to dedisperse.
         */
        template<typename T
                ,typename data::EnableIfIsTimeFrequency<T, bool> = true>
        void operator()(std::shared_ptr<T> const& data);

};

} // namespace sps
} // namespace cheetah
} // namespace ska
#include "cheetah/sps/detail/Sps.cpp"

#endif // SKA_CHEETAH_SPS_SPS_H
