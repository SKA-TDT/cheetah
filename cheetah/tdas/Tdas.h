/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_TDAS_TDAS_H
#define SKA_CHEETAH_TDAS_TDAS_H

#include "cheetah/tdas/Config.h"
#include "cheetah/tdas/cuda/Tdas.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/detail/DmTimeSlice.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/DmTrials.h"
#include "panda/ConfigurableTask.h"
#include "panda/AlgorithmTuple.h"
#include "panda/Log.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace tdas {

typedef data::DmTrials<cheetah::Cpu,float> DmTrialsType;
typedef data::DmTime<DmTrialsType> DmTimeType;
typedef data::detail::DmTimeSlice<DmTimeType> DmTimeSliceType;

/**
 * @brief      Top-level synchronous interface for the Tdas module
 *
 * @details     Tdas stands for time-domain acceleration search. Here we search
 *             for significant periodic signal at multiple accelerations using
 *             the technique of time domain resampling + FFT.
 *
 * @tparam     T     The value type to use for processing (float or double)
 */
template <typename T>
class TdasBase
{
    private:
        typedef panda::AlgorithmTuple<cuda::Tdas<T>> Implementations;

    public:

        /**
         * @brief      Construct a new TdasBase instance
         *
         * @param      config  The algorithm configuration
         */
        TdasBase(Config const& config);
        TdasBase(TdasBase const&) = delete;
        TdasBase(TdasBase&&) = default;
        ~TdasBase();

        /**
         * @brief      Process a DmTimeSlice in search of significant periodic
         *             signals over a range of acceleration values.
         *
         * @details     This is a forwarding interface that will forward the call to the
         *             relevant implementation based on the provided arguments.
         *
         * @param      resource   The resource to process on
         * @param      data       The input DmTimeSlice
         * @param[in]  args       Additional arguments to be passed to the implementation
         *
         * @tparam     Arch       The architecture to process on
         * @tparam     Args       The types of any additional arguments
         *
         * @return     A shared pointer to a list of candidate signals
         */
        template <typename Arch, typename... Args>
        std::shared_ptr<data::Ccl> process(panda::PoolResource<Arch>& resource,
            DmTimeSliceType const& data,
            Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;
};

/**
 * @brief      Async + Sync mixed interface for Tdas
 *
 * @tparam     T        The value type to use for processing (float or double)
 * @tparam     Handler  The type of the Handler that will be used for async calls
 */
template <typename T, typename Handler=void>
class Tdas: public TdasBase<T>
{
    public:

        /**
         * @brief      Construct a new Tdas instance
         *
         * @param      config  The algorithm configuration
         */
        Tdas(ConfigType const& config, Handler& handler);
        Tdas(Tdas const&) = delete;
        Tdas(Tdas&&) = default;
        ~Tdas();

        /**
         * @brief      Async call to tdas
         *
         * @param      data  A DmTime to be processed
         */
        void operator()(std::shared_ptr<DmTimeType> const& data);

    private:
        ConfigType const& _config;
        panda::ConfigurableTask<typename Config::PoolType, Handler&, std::shared_ptr<DmTimeSliceType>> _task;
};

/**
 * @brief      Sync-only interface for Tdas
 *
 * @tparam     T     The value type to use for processing (float or double)
 */
template <typename T>
class Tdas<T,void>: public TdasBase<T>
{
    using TdasBase<T>::TdasBase;
};


} // namespace tdas
} // namespace cheetah
} // namespace ska

#include "cheetah/tdas/detail/Tdas.cpp"

#endif // SKA_CHEETAH_TDAS_TDAS_H
