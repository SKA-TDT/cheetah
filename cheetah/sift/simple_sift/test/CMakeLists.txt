include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_sift_simpleSift_src
    src/SiftTest.cpp
    src/gtest_sift_simple_sift.cpp
)

add_executable(gtest_sift_simpleSift ${gtest_sift_simpleSift_src})
target_link_libraries(gtest_sift_simpleSift ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_sift_simpleSift gtest_sift_simpleSift)
