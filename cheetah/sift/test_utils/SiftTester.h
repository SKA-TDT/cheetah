/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SIFT_TEST_SIFTTESTER_H
#define SKA_CHEETAH_SIFT_TEST_SIFTTESTER_H

#include "cheetah/sift/Config.h"
#include "cheetah/sift/Sift.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "panda/AlgorithmInfo.h"
#include "panda/test/TestHandler.h"

#include <gtest/gtest.h>
#include <memory>

namespace ska {
namespace cheetah {
namespace sift {
namespace test {

/**
 * @brief
 * Generic functional test for the SiftTester algorithm
 *
 * @details
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requirements it needs to run.
 *
 * e.g.
 * @code
 * struct SimpleSiftTraits : public SiftTesterTraits<simple_sift::Sift::Architecture, simple_sift::Sift::ArchitectureCapability>
 * {
 *   typedef test::SiftTesterTraits<simple_sift::Sift::Architecture, typename simple_sift::Sift::ArchitectureCapability> BaseT;
 *   typedef Sift::Architecture Arch;
 *   typedef typename BaseT::DeviceType DeviceType;
 * };
 * @endcode
 *
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_CASE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<SimpleSiftTraits, OpenClTraits> MyTypes;
 * INSTANTIATE_TYPED_TEST_CASE_P(MyAlgo, SiftTester, MyTypes);
 * @endcode
 *  n.b. the INSTANTIATE_TYPED_TEST_CASE_P must be in the same namespace as this class
 *
 */

template<typename SiftAlgo>
struct SiftTesterTraits : public utils::test::PoolAlgorithmTesterTraits<typename SiftAlgo::Architecture
                                                                      , typename panda::AlgorithmInfo<SiftAlgo>::ArchitectureCapability>
{
    private:
        typedef utils::test::PoolAlgorithmTesterTraits<typename SiftAlgo::Architecture
                                                     , typename panda::AlgorithmInfo<SiftAlgo>::ArchitectureCapability> BaseT;

    public:
        typedef typename BaseT::PoolType PoolType;

    private:
        struct SiftHandler : public panda::test::TestHandler
        {
            private:
                typedef panda::test::TestHandler BaseT;

            public:
                SiftHandler() = default;
                SiftHandler(SiftHandler const&) = delete;

                void operator()(std::shared_ptr<data::Scl>);
                std::shared_ptr<data::Scl> const& data() const { return _data; }

            private:
                std::shared_ptr<data::Scl> _data;
        };

        struct TestConfig : public sift::Config
        {
                typedef typename SiftTesterTraits::PoolType PoolType;

            public:
                TestConfig() : _pool(nullptr)
                {
                    deactivate_all();
                }
                PoolType& pool() const { assert(_pool); return *_pool; }
                void pool(PoolType& pool) { _pool = &pool; }

            protected:
                PoolType* _pool;
        };

    public:
        typedef sift::Sift<SiftHandler&, TestConfig> Api;

    public:
        SiftTesterTraits() = default;
        Api& api(PoolType&);
        TestConfig& config();
        SiftHandler& handler();

    private:
        TestConfig _config;
        SiftHandler _handler;
        std::unique_ptr<Api> _api;
};

template <typename TestTraits>
class SiftTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        SiftTester();
        ~SiftTester();

    private:
};

TYPED_TEST_CASE_P(SiftTester);

} // namespace test
} // namespace sift
} // namespace cheetah
} // namespace ska

#include "cheetah/sift/test_utils/detail/SiftTester.cpp"

#endif // SKA_CHEETAH_SIFT_TEST_SIFTTESTER_H
