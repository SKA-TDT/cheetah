#include "cheetah/ddtr/astroaccelerate/test/DedispersionStrategyTest.h"
#include "cheetah/ddtr/astroaccelerate/DedispersionStrategy.h"
#include "cheetah/ddtr/DedispersionTrialPlan.h"
#include <pss/astrotypes/units/Units.h>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {
namespace test {

template<typename T>
DedispersionStrategyTest<T>::DedispersionStrategyTest()
    : ::testing::Test()
{
}

template<typename T>
DedispersionStrategyTest<T>::~DedispersionStrategyTest()
{
}

template<typename T>
void DedispersionStrategyTest<T>::SetUp()
{
}

template<typename T>
void DedispersionStrategyTest<T>::TearDown()
{
}

template <typename NumericalRep>
class TestDedispersionOptimizationParameters{
    public:
        static constexpr unsigned UNROLLS = 8;
        static constexpr unsigned SNUMREG = 8;
        static constexpr unsigned SDIVINT = 14;
        static constexpr unsigned SDIVINDM = 50;
        static constexpr float SFDIVINDM = 50.0f;
        static constexpr unsigned MIN_DMS_PER_SPS_RUN = 64;
};

typedef ::testing::Types<unsigned char, unsigned short, float> NumericTypes;
TYPED_TEST_CASE(DedispersionStrategyTest, NumericTypes);

TYPED_TEST(DedispersionStrategyTest, test_DedispersionStrategy_constructor)
{
    typedef TypeParam NumericRep;

    data::DimensionSize<data::Time> number_of_spectra(128*1024U);
    data::DimensionSize<data::Frequency> number_of_channels(512U);

    data::TimeFrequency<Cpu,NumericRep> tf(number_of_spectra, number_of_channels);
    auto f1 =  data::FrequencyType(1000.0 * pss::astrotypes::units::megahertz);
    auto f2 =  data::FrequencyType(1500.0 * pss::astrotypes::units::megahertz);
    auto delta = (f2 - f1)/ (double)number_of_channels;
    tf.set_channel_frequencies_const_width( f1, delta );
    tf.sample_interval(0.001*data::second);
    ddtr::DedispersionTrialPlan plan("test");
    plan.dedispersion_samples(1<<14);
    ASSERT_EQ(1<<14, plan.dedispersion_samples());

    ASSERT_DOUBLE_EQ(0.0, plan.max_dm().value());

    plan.add_dm_range(40 * data::parsecs_per_cube_cm, 100 * data::parsecs_per_cube_cm, 20 * data::parsecs_per_cube_cm);
    ASSERT_DOUBLE_EQ(100.0, plan.max_dm().value());

    plan.add_dm_range(0 * data::parsecs_per_cube_cm, 40 * data::parsecs_per_cube_cm, 20 * data::parsecs_per_cube_cm); // should not affect the max dm
    ASSERT_DOUBLE_EQ(100.0, plan.max_dm().value());

    DedispersionStrategy<Cpu, NumericRep> ds(tf,plan,128*1024*1024);
    ASSERT_EQ(ds.nchans(), 512);
    ASSERT_EQ(ds.nsamp(), plan.dedispersion_samples());
    ASSERT_EQ(ds.tsamp(), 0.001*data::seconds);

}

TYPED_TEST(DedispersionStrategyTest, test_make_strategy)
{
    typedef TypeParam NumericRep;
    data::DimensionSize<data::Time> number_of_spectra(128*1024U);
    data::DimensionSize<data::Frequency> number_of_channels(512U);

    data::TimeFrequency<Cpu,NumericRep> tf(number_of_spectra, number_of_channels);
    auto f1 =  data::FrequencyType(1000.0 * pss::astrotypes::units::megahertz);
    auto f2 =  data::FrequencyType(1500.0 * pss::astrotypes::units::megahertz);
    auto delta = (f2 - f1)/ (double)number_of_channels;
    tf.set_channel_frequencies_const_width( f1, delta );
    tf.sample_interval(0.001*data::second);

    ddtr::DedispersionTrialPlan plan("test");
    plan.dedispersion_samples(1<<14);
    ASSERT_EQ(1<<14, plan.dedispersion_samples());

    ASSERT_DOUBLE_EQ(0.0, plan.max_dm().value());

    plan.add_dm_range(40 * data::parsecs_per_cube_cm, 100 * data::parsecs_per_cube_cm, 20 * data::parsecs_per_cube_cm);
    ASSERT_DOUBLE_EQ(100.0, plan.max_dm().value());

    plan.add_dm_range(0 * data::parsecs_per_cube_cm, 40 * data::parsecs_per_cube_cm, 20 * data::parsecs_per_cube_cm); // should not affect the max dm
    ASSERT_DOUBLE_EQ(100.0, plan.max_dm().value());

    plan.add_dm_range(400 * data::parsecs_per_cube_cm, 500 * data::parsecs_per_cube_cm, 10 * data::parsecs_per_cube_cm);
    ASSERT_DOUBLE_EQ(500.0, plan.max_dm().value());

    typedef DedispersionStrategy<Cpu, NumericRep,TestDedispersionOptimizationParameters<NumericRep>> DedispersionStrategyType;
    DedispersionStrategyType ds(tf,plan,128*1024*1024);
    ASSERT_EQ(ds.nchans(), 512);
    for(unsigned i=0; i<ds.range(); ++i)
        ASSERT_GT(ds.ndms()[i],TestDedispersionOptimizationParameters<NumericRep>::SDIVINDM-1);

}

} // namespace test
} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
