/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_TDAS_CONFIG_H
#define SKA_CHEETAH_TDAS_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/tdas/cuda/Config.h"
#include "cheetah/tdas/AccListGenConfig.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "panda/PoolSelector.h"

#include <vector>

namespace ska {
namespace cheetah {
namespace tdas {

/**
 * @brief
 *
 * @details
 *
 */

class Config : public cheetah::utils::Config
{
    public:
        Config();
        ~Config();

        /**
         * @brief Configuration details for the cuda based RFIM algorithm
         */
        cuda::Config const& cuda_config() const;
        cuda::Config& cuda_config();

        /**
         * @brief      Return acceleration list generator configuration
         */
        AccListGenConfig const& acceleration_list_generator() const;
        AccListGenConfig& acceleration_list_generator();

        /**
         * @brief      The number or DM trials to process in each Tdas async task
         */
        std::size_t dm_trials_per_task() const;
        void dm_trials_per_task(std::size_t ntrials);

        /**
         * @brief      The minimum timeseries length that will be searched
         */
        std::size_t minimum_size() const;
        void minimum_size(std::size_t minimum_size);

        /**
         * @brief      The size for transform to use for the search
         *
         * @details     For all time series lengths > minimum_size the time series
         *             to be searched will be padded out to the length specified by
         *             size.
         */
        std::size_t size() const;
        void size(std::size_t size);

        /**
         * @brief      Number of harmonic sums to perform in the search
         */
        void number_of_harmonic_sums(std::size_t nharmonics);
        std::size_t number_of_harmonic_sums() const;

        /**
         * @brief: check if the module is active
         */ 
        bool active() const;

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        bool _active;
        cuda::Config _cuda_config;
        AccListGenConfig _acc_gen_config;
        std::size_t _dm_trials_per_task;
        std::size_t _size;
        std::size_t _minimum_size;
        std::size_t _nharmonics;
};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace tdas
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_TDAS_CONFIG_H
