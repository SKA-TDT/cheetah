#include "cheetah/data/test_utils/FrequencySeriesTest.h"
#include "cheetah/data/test_utils/FrequencySeriesTester.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {

typedef ::testing::Types<
    FrequencySeriesTestTraits<cheetah::Cuda,float>,
    FrequencySeriesTestTraits<cheetah::Cuda,double>,
    FrequencySeriesTestTraits<cheetah::Cuda,char>
    > FrequencySeriesTestTypes;
TYPED_TEST_CASE(FrequencySeriesTest, FrequencySeriesTestTypes);

TYPED_TEST(FrequencySeriesTest, test_bins)
{
    SampleCountTest<TypeParam>::test(1000L);
    SampleCountTest<TypeParam>::test(1<<23);
}

template<typename ValueType>
class CudaFrequencySeriesTesterTraits : public FrequencySeriesTesterTraits<data::FrequencySeries<cheetah::Cuda, ValueType>>
{
        typedef FrequencySeriesTesterTraits<data::FrequencySeries<cheetah::Cuda, ValueType>> BaseT;
        typedef typename BaseT::Allocator Allocator;
};


typedef ::testing::Types<CudaFrequencySeriesTesterTraits<float>, CudaFrequencySeriesTesterTraits<uint8_t>> CudaFrequencySeriesTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(BasicCudaSeries, FrequencySeriesTester, CudaFrequencySeriesTraitsTypes);




} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
