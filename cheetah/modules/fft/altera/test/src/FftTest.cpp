#include "cheetah/modules/fft/altera/test/FftTest.h"
#include "cheetah/modules/fft/test_utils/FftTester.h"
#include "cheetah/modules/fft/altera/Fft.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace altera {
namespace test {

/**
 * @details Unit tests for FFT Plan generations for different combinations of FFT type and data length
 * R2C type:
 *  case 1   input length lesser than round of sample size that is even power of 2
 *  case 2   input length greater than round of sample size that is even power of 2
 *  case 3   input length lesser than round of sample size that is odd power of 2 but exceeds _max_padding
 *  case 4   input length lesser than round of sample size that is odd power of 2
 *  case 5   input length sample size that is odd power of 2
 *  case 6   input length greater than round of sample size that is odd power of 2
 * C2C type:
 *  case 1   input length lesser than round of sample size that is odd power of 2
 *  case 2   input length greater than round of sample size that is odd power of 2
 *  case 3   input length lesser than round of sample size that is even power of 2 but exceeds _max_padding
 *  case 4   input length lesser than round of sample size that is even power of 2
 *  case 5   input length sample size that is even power of 2
 *  case 6   input length greater than round of sample size that is even power of 2
 */

// R2C case 1
TYPED_TEST(FftTest, altera_fft_r2c_test_1000)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, TypeParam> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1000);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 512);
}

// R2C case 2
TYPED_TEST(FftTest, altera_fft_r2c_test_1100)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, TypeParam> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1100);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 512);
}

// R2C case 3
TYPED_TEST(FftTest, altera_fft_r2c_test_2000)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, TypeParam> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(2000);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 512);
}

// R2C case 4
TYPED_TEST(FftTest, altera_fft_r2c_test_2040)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, TypeParam> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(2040);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 2048);
}

// R2C case 5
TYPED_TEST(FftTest, altera_fft_r2c_test_2048)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, TypeParam> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(2048);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 2048);
}

// R2C case 6
TYPED_TEST(FftTest, altera_fft_r2c_test_2148)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, TypeParam> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(2148);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 2048);
}

// C2C case 1
TYPED_TEST(FftTest, altera_fft_c2c_fwd_test_500)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(500);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 256);
}

// C2C case 2
TYPED_TEST(FftTest, altera_fft_c2c_fwd_test_520)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(520);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 256);
}

// C2C case 3
TYPED_TEST(FftTest, altera_fft_c2c_fwd_test_1000)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1000);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 256);
}

// C2C case 4
TYPED_TEST(FftTest, altera_fft_c2c_fwd_test_1015)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1015);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2C case 5
TYPED_TEST(FftTest, altera_fft_c2c_fwd_test_1024)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1024);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2C case 6
TYPED_TEST(FftTest, altera_fft_c2c_fwd_test_1124)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::TimeSeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::FrequencySeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1124);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2C case 1 (inv)
TYPED_TEST(FftTest, altera_fft_c2c_inv_test_500)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::FrequencySeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::TimeSeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(500);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 256);
}

// C2C case 2 (inv)
TYPED_TEST(FftTest, altera_fft_c2c_inv_test_520)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::FrequencySeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::TimeSeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(520);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 256);
}

// C2C case 3 (inv)
TYPED_TEST(FftTest, altera_fft_c2c_inv_test_1000)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::FrequencySeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::TimeSeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1000);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 256);
}

// C2C case 4 (inv)
TYPED_TEST(FftTest, altera_fft_c2c_inv_test_1015)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::FrequencySeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::TimeSeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1015);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2C case 5 (inv)
TYPED_TEST(FftTest, altera_fft_c2c_inv_test_1024)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::FrequencySeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::TimeSeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1024);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

// C2C case 6 (inv)
TYPED_TEST(FftTest, altera_fft_c2c_inv_test_1124)
{
    using ComplexT = typename data::ComplexTypeTraits<Cpu,TypeParam>::type;
    typedef data::FrequencySeries<Cpu, ComplexT> InputData;
    typedef panda::TypeTag<data::TimeSeries<Cpu, ComplexT>> OutputDataTag;
    Config config;
    Fft fft(config);
    InputData input(1124);
    Plan plan = fft.plan(input, OutputDataTag());
    ASSERT_EQ(plan.fft_size(), 1024);
}

template<typename NumericalT>
struct AlteraTraits
    : public fft::test::FftTesterTraits<fft::altera::Fft, NumericalT>
{
    typedef fft::test::FftTesterTraits<fft::altera::Fft, NumericalT> BaseT;
    typedef typename BaseT::DeviceType DeviceType;

};

} // namespace test
} // namespace altera
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {
namespace test {

//typedef ::testing::Types<altera::test::AlteraTraits<uint8_t>, altera::test::AlteraTraits<uint16_t>, altera::test::AlteraTraits<float>> AlteraTraitsTypes;
typedef ::testing::Types<altera::test::AlteraTraits<float>> AlteraTraitsTypes;
#ifdef SKA_CHEETAH_ENABLE_OPENCL
INSTANTIATE_TYPED_TEST_CASE_P(Fpga, FftTester, AlteraTraitsTypes);
#endif // SKA_CHEETAH_ENABLE_OPENCL

} // namespace test
} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska
