/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PWFT_TEST_PWFTTESTER_H
#define SKA_CHEETAH_PWFT_TEST_PWFTTESTER_H

#include "cheetah/pwft/Config.h"
#include "cheetah/pwft/Pwft.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/Units.h"

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace pwft {
namespace test {

/**
 * @brief
 * Generic functional test for the PwftTester algorithm
 *
 * @details
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requiremnst it needs to run.
 *
 * e.g.
 * @code
 * struct CudaTraits : public PwftTesterTraits<cuda::Pwft::Architecture,cuda::Pwft::ArchitectureCapability>
 * {
 *   typedef test::PwftTesterTraits<cuda::Pwft::Architecture, typename cuda::Pwft::ArchitectureCapability> BaseT;
 *   typedef Pwft::Architecture Arch;
 *   typedef typename BaseT::DeviceType DeviceType;
 * };
 * @endcode
 *
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_CASE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<CudaTraits, OpenClTraits> MyTypes;
 * INSTANTIATE_TYPED_TEST_CASE_P(MyAlgo, PwftTester, MyTypes);
 * @endcode
 *  n.b. the INSTANTIATE_TYPED_TEST_CASE_P must be in the same namespace as this class
 *
 */


template<typename ArchitectureTag, typename ArchitectureCapability>
struct PwftTesterTraits
    : public utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability>
{
    public:
        typedef utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability> BaseT;
        typedef ArchitectureTag Arch;
        typedef typename BaseT::DeviceType DeviceType;

    public:
        PwftTesterTraits();
        pwft::Pwft& api();

    private:
        pwft::Config _config;
        pwft::Pwft _api;

};


template <typename TestTraits>
class PwftTester
    : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        PwftTester();
        ~PwftTester();

    private:
};

TYPED_TEST_CASE_P(PwftTester);

} // namespace test
} // namespace pwft
} // namespace cheetah
} // namespace ska

#include "cheetah/pwft/test_utils/detail/PwftTester.cpp"


#endif // SKA_CHEETAH_PWFT_TEST_PWFTTESTER_H
