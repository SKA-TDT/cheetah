/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PWFT_CUDA_PWFT_H
#define SKA_CHEETAH_PWFT_CUDA_PWFT_H

#include "cheetah/Configuration.h"

#ifdef SKA_CHEETAH_ENABLE_CUDA
#include "cheetah/pwft/cuda/Pwft.cuh"
#else


#include "cheetah/pwft/cuda/Config.h"
#include "cheetah/pwft/Config.h"
#include "cheetah/utils/Mock.h"

namespace ska {
namespace cheetah {
namespace pwft {
namespace cuda {

/**
 * Mock interface for Pwft to support non-CUDA builds
 */
using Pwft = utils::Mock<cheetah::Cuda, Config const&, pwft::Config const&>;

} //cuda
} //pwft
} //cheetah
} //ska
#endif //SKA_CHEETAH_ENABLE_CUDA

#endif //SKA_CHEETAH_PWFT_CUDA_PWFT_H
