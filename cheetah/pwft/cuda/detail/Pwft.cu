#include "cheetah/pwft/cuda/Pwft.cuh"
#include "panda/Log.h"

#define RSQRT2 0.70710678118654746f

namespace ska {
namespace cheetah {
namespace pwft {
namespace cuda {
namespace detail {

/**
 * @brief Thrust functor for calculating the power of a complex spectrum
 */
template <typename T>
struct FormPower: public thrust::unary_function<T,thrust::complex<T> >
{
    inline __host__ __device__
    T operator()(const thrust::complex<T> &x) const
    {
        T val = thrust::abs<T>(x);
        return val*val;
    }
};

/**
 * @brief Thrust functor for calculating the power of a complex
 *spectrum using nearest neighbours
 */
template <typename T>
struct FormPowerNN: public thrust::binary_function<T, thrust::complex<T>, thrust::complex<T> >
{
    inline __host__ __device__
    T operator()(const thrust::complex<T> &x, const thrust::complex<T> &y) const
    {
        T val = thrust::max<T>(thrust::abs<T>(x-y)*RSQRT2,thrust::abs<T>(x));
        return val*val;
    }
};

} //namespace detail


template <typename T, typename InputAlloc, typename OutputAlloc>
void Pwft::process_direct(ResourceType& gpu,
    data::FrequencySeries<Architecture,typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,InputAlloc> const& input,
    data::PowerSeries<Architecture,T,OutputAlloc>& output)
{
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    output.resize(input.size());
    output.frequency_step(input.frequency_step());
    output.degrees_of_freedom(2.0);
    thrust::transform(thrust::cuda::par,
        input.begin(),
        input.end(),output.begin(),
        detail::FormPower<T>());
}

template <typename T, typename InputAlloc, typename OutputAlloc>
void Pwft::process_nn(ResourceType& gpu,
    data::FrequencySeries<Architecture,typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,InputAlloc> const& input,
    data::PowerSeries<Architecture,T,OutputAlloc>& output) //cuda::ExecutionPolicy& policy)
{
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    output.resize(input.size());
    output.frequency_step(input.frequency_step());
    output.degrees_of_freedom(2.0+RSQRT2);
    output[0] = 0;
    thrust::transform(thrust::cuda::par,
        input.begin()+1, input.end(),
        input.begin(), output.begin()+1,
        detail::FormPowerNN<T>());
}

} //cuda
} //pwft
} //cheetah
} //ska