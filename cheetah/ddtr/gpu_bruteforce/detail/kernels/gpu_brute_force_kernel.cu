/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/gpu_bruteforce/detail/GpuConstraints.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

__constant__ __device__ float dmshifts[GpuConstraints::max_constant_size];

template<typename NumericalT>
__global__ void gpu_brute_force_kernel( NumericalT *d_input
                                      , float *d_output
                                      , int nchans
                                      , int dsamps
                                      , int nsamps
                                      , float startdm
                                      , float dmstep
                                      , int ndms
                                      , int bin)
{
    float temp=0.0;
    int t = blockIdx.x * blockDim.x + threadIdx.x;
    int shift=0;
    if(t<dsamps)
    {
        for(int dm=0; dm<ndms; dm++)
        {
            temp=0.0;
            for(int i=0; i<nchans; i++)
            {
                shift = (int)(dmshifts[i]*(startdm+dm*dmstep)/bin);
                for(int b=0; b<bin; b++)
                {
                    temp += (float)(d_input[nsamps*i+t+shift]);
                }
            }
            d_output[dsamps*dm+t] = temp/nchans;
        }
    }
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace cheetah
} // namespace ska
