/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_CPU_DEDISPERSIONPLAN_H
#define SKA_CHEETAH_DDTR_CPU_DEDISPERSIONPLAN_H

#include "cheetah/data/TimeFrequency.h"
#include "cheetah/ddtr/Config.h"
#include "cheetah/ddtr/cpu/Config.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {

/**
 * @brief
 * @details
 */
template <typename DdtrTraits>
class DedispersionPlan
{
    public:
        typedef typename DdtrTraits::value_type NumericalT;
        typedef typename DdtrTraits::DedispersionHandler DedispersionHandler;
        typedef typename DdtrTraits::DmTrialsType DmTrialsType;
        typedef typename DdtrTraits::TimeFrequencyType TimeFrequencyType;
        typedef ddtr::Config::Dm Dm;
        typedef typename DdtrTraits::BufferFillerType BufferFillerType;
        typedef typename DdtrTraits::BufferType BufferType;
        typedef std::vector<ddtr::Config::Dm> DmListType;
        typedef typename TimeFrequencyType::FrequencyType FrequencyType;
        typedef typename TimeFrequencyType::TimeType TimeType;
        typedef std::vector<FrequencyType> FrequencyListType;
        typedef DedispersionTrialPlan ConfigType;

    public:
        /**
         * @param max_data_elements The maximum memory available on the device (in number of DdtrTraits::value_type values)
         */
        DedispersionPlan(ConfigType const& config, std::size_t max_data_elements);
        ~DedispersionPlan();

        /**
         * @brief reset the plan to be compatible with the TimeFrequency metadata
         * @details note that the number_of_spectra of this TimeFrequencyType object is ignored
         */
        data::DimensionSize<data::Time> reset(TimeFrequencyType const&);

        /**
         * @brief reset the plan to be compatible with the value
         */
        void reset(data::DimensionSize<data::Time> const& spectra);

        /**
         * @brief return the number of spectra currently configured
         */
        data::DimensionSize<data::Time> number_of_spectra() const;

        /**
         * @brief return the number of spectra in the overlap buffer
         * @details Corresponds to the maximum delay required for the max dm value in the plan
         */
        data::DimensionSize<data::Time> buffer_overlap() const;


        /**
         * @brief return algo specific dm_trials
         */
        std::vector<Dm> const& dm_trials() const;

        /**
         * @brief return algo specific dm_factors
         */
        std::vector<double> const& dm_factors() const;

        /**
         * @brief return a DmTrialsMetadata block consistent with the plan and the incoming data parameters
         */
        std::shared_ptr<data::DmTrialsMetadata> dm_trials_metadata(data::TimeFrequencyMetadata const& tf_info, data::DimensionSize<data::Time> number_of_spectra);

    protected:
        std::shared_ptr<data::DmTrialsMetadata> generate_dmtrials_metadata(TimeType sample_interval, data::DimensionSize<data::Time> nspectra, std::size_t nsamples) const;

    private:
        ConfigType const& _algo_config;
        std::size_t _memory;
        data::DimensionSize<data::Time> _max_delay;
        data::DimensionSize<data::Time> _number_of_spectra;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        std::vector<double> _dm_factors;
};

} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska

#include "cheetah/ddtr/cpu/detail/DedispersionPlan.cpp"
#endif // SKA_CHEETAH_DDTR_CPU_DEDISPERSIONPLAN_H
